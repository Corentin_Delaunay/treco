<?php
    session_start();
    include("metier/Connexion.php");
    if(!isset($_SESSION['etat'])){$_SESSION['etat']="deconnecte";}
    if(!isset($_SESSION['type'])){$_SESSION['type']=null;}
    if(!isset($_SESSION['id'])){$_SESSION['id']=null;}
    $_SESSION['page_actuelle']="index.php";
    ?>

<!DOCTYPE html>
<html>
        <head>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width" />
            <link href="style/bootstrap.css" rel="stylesheet"/>
            <link href="style/presentation.css" rel="stylesheet"/>
            <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
            <title>TRECO, trouvez la formation qui vous correspond</title>
            <link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico" />
            <!-- Global site tag (gtag.js) - Google Analytics -->
            <script async src="https://www.googletagmanager.com/gtag/js?id=UA-119675621-1"></script>
            <script>
               window.dataLayer = window.dataLayer || [];
               function gtag(){dataLayer.push(arguments);}
               gtag('js', new Date());

               gtag('config', 'UA-119675621-1');
            </script>
            <script type="text/javascript"> _linkedin_data_partner_id = "374204"; </script><script type="text/javascript"> (function(){var s = document.getElementsByTagName("script")[0]; var b = document.createElement("script"); b.type = "text/javascript";b.async = true; b.src = "https://snap.licdn.com/li.lms-analytics/insight.min.js"; s.parentNode.insertBefore(b, s);})(); </script> <noscript> <img height="1" width="1" style="display:none;" alt="" src="https://dc.ads.linkedin.com/collect/?pid=374204&fmt=gif
https://dc.ads.linkedin.com/collect/?pid=374204&fmt=gif
" /> </noscript>
        </head>

        <header class="site-header">
                <div class="container">
                    <div class="row" id="entete">
                        <div class="col-lg-7">
                            <a href="index.php"><img id="img_tete" src="images/TRECO-white.png" width="30%"/></a>
                        </div>
                        <div class="row col-lg-5 mx-auto navigation">
                            <div class="col-lg-1 my-auto mx-auto">
                                <a id="menu" href="index.php">Accueil</a>
                            </div>
                            <div class="col-lg-1 my-auto mx-auto">
                                <a id="menu" href="metier/organisme-de-formation.php">Organisme</a>
                            </div>
                            <div class="col-lg-1 my-auto mx-auto">
                                <a id="menu" href="metier/Contacts.php">Contacts</a>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
        
        <body>
            <div id="hero-banner">
                <div class="container">
                    <div class="row title">
                        <h1><b>TROUVEZ LA FORMATION QUI VOUS CORRESPOND !</b></h1>
                    </div>
                    <div class="row justify-content-center" id="recherche">
                        <div class="col-8">
                            <form method="post" action="metier/Recherche.php" class="recherche">
                                <div class="row">
                                    <div class="col-10">
                                        <input id="fonctionR" type="text" name="name" placeholder="Entrez votre recherche" />
                                    </div>
                                    <div class="col-2">
                                        <button type="submit" class="btn btn-primary button-R"><i class="fas fa-search"></i><span class="d-none d-lg-inline-block">&nbsp;Rechercher</span></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <div class="content">
                <div class="container">
                    <div class="row justify-content-start m-auto">
                        <div class="col-12" id="enseigne">
                            <h1>Les meilleures formations de design</h1>
                        </div>
                    </div>
                    <div class="row justify-content-start m-auto">
                        <div class="col-12">
                            <div class="row m-auto">
                                <?php
                                $requeteSql_nBFormation = $bdd->query('SELECT COUNT(*) as cpt FROM t_formation WHERE ID_D2 = 20');
                                $nb_formation = $requeteSql_nBFormation->fetch();
                                $requeteSql_formation = $bdd->query('SELECT * FROM t_formation WHERE ID_D2 = 20');

                                $nb_formation = $nb_formation['cpt'];
                                $cpt = 0;
                                while($donnees_formation = $requeteSql_formation->fetch())
                                {
                                    if($nb_formation-$cpt<5){
                                        echo '<div class="col-4 formation" onClick="javascript:window.open(\'metier/fiche-formation.php?numero='.$donnees_formation['ID_FORM'].'&nom='.$donnees_formation['TITRE'].'\');">';
                                        echo '<img src="images/marketing,%20communication.jpg" width="100%" />';
                                        echo '<h3>'.$donnees_formation['TITRE'].'</h3>';
                                        $requeteSql_description = $bdd->query('SELECT * FROM t_description WHERE ID_DESCRIPTION='.$donnees_formation['ID_DESCRIPTION']);
                                        $donnees_description = $requeteSql_description->fetch();
                                        //echo '<p>'.$donnees_description['CHAPEAU'].'</p>';
                                        echo '</div>';
                                    }
                                    $cpt++;
                                }
                                ?>
                            </div>
                        </div>
                    </div>

                    <div class="row justify-content-start m-auto">
                        <div class="col-12" id="enseigne">
                            <h1>Les meilleures formations Web</h1>
                        </div>
                    </div>
                    <div class="row justify-content-start m-auto">
                        <div class="col-12">
                            <div class="row m-auto">
                                <?php
                                $requeteSql_nBFormation = $bdd->query('SELECT COUNT(*) as cpt FROM t_formation WHERE ID_D2 = 80');
                                $nb_formation = $requeteSql_nBFormation->fetch();
                                $requeteSql_formation = $bdd->query('SELECT * FROM t_formation WHERE ID_D2 = 80');

                                $nb_formation = $nb_formation['cpt'];
                                $cpt = 0;
                                while($donnees_formation = $requeteSql_formation->fetch())
                                {
                                    if($nb_formation-$cpt<5 && $nb_formation-$cpt>0){
                                        echo '<div class="col-4 formation" onClick="javascript:window.open(\'metier/fiche-formation.php?numero='.$donnees_formation['ID_FORM'].'&nom='.$donnees_formation['TITRE'].'\');">';
                                        echo '<img src="images/Informatique.jpg" width="100%" />';
                                        echo '<h3>'.$donnees_formation['TITRE'].'</h3>';
                                        $requeteSql_description = $bdd->query('SELECT * FROM t_description WHERE ID_DESCRIPTION='.$donnees_formation['ID_DESCRIPTION']);
                                        $donnees_description = $requeteSql_description->fetch();
                                        //echo '<p>'.$donnees_description['CHAPEAU'].'</p>';
                                        echo '</div>';
                                    }
                                    $cpt++;
                                }
                                ?>
                            </div>
                        </div>
                    </div>

                    <div class="row justify-content-start m-auto">
                        <div class="col-12" id="enseigne">
                            <h1>Les formations les plus suivies</h1>
                        </div>
                    </div>
                    <div class="row justify-content-start m-auto">
                        <div class="col-12">
                            <div class="row m-auto">
                                <?php
                                $requeteSql_nBFormation = $bdd->query('SELECT COUNT(*) as cpt FROM t_formation');
                                $nb_formation = $requeteSql_nBFormation->fetch();
                                $requeteSql_formation = $bdd->query('SELECT * FROM t_formation');
                     
                                $nb_formation = $nb_formation['cpt'];
                                $random1 = rand(1,$nb_formation-1);
                                $random2 = rand(1,$nb_formation-1);
                                while($random2 == $random1) $random2 = rand(1,$nb_formation-1);
                                $random3 = rand(1,$nb_formation-1);
                                while($random3 == $random2 || $random3 == $random1) $random3 = rand(1,$nb_information-1);
                                $random4 = rand(1,$nb_formation-1);
                                while($random4 == $random3 || $random4 == $random2 || $random4 == $random1) $random4 = rand(1,$nb_formation-1);
                                $random = array($random1,$random2,$random3,$random4);
                                sort($random);
                                $cpt = 0;
                                $index = 0;
                                while($donnees_formation = $requeteSql_formation->fetch())
                                {
                                    if($cpt == $random[$index]){
                                        echo '<div class="col-4 formation" onClick="javascript:window.open(\'metier/fiche-formation.php?numero='.$donnees_formation['ID_FORM'].'&nom='.$donnees_formation['TITRE'].'\');">';
                                        echo '<img src="images/management.jpg" width="100%" />';
                                        echo '<h3>'.$donnees_formation['TITRE'].'</h3>';
                                        $requeteSql_description = $bdd->query('SELECT * FROM t_description WHERE ID_DESCRIPTION='.$donnees_formation['ID_DESCRIPTION']);
                                        $donnees_description = $requeteSql_description->fetch();
                                        //echo '<p>'.$donnees_description['CHAPEAU'].'</p>';
                                        echo '</div>';
                                        $index++;
                                    }
                                    $cpt++;
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        <!--<div>
             <a href="metier/ajouter-formation.php">&nbsp;</a>
        </div>-->

        <?php $bdd = null;?>
    
    </body>

    <footer>
            <div class="container">
                <div class="row m-auto">
                    <div class="col-lg-7 m-auto archi">
                        <div class="row justify-content-between grand-titre">
                            <div class="col"><h3>Redirection</h3></div>
                            <div class="col"><h3>Préférences</h3></div>
                            <div class="col"><h3>Support</h3></div>
                        </div>
                        <div class="row justify-content-between">
                            <div class="col"><a href="#"><p class="texte">Accueil</p></a></div>
                            <div class="col"><p class="texte">Web</p></div>
                            <div class="col"><p class="texte">Contact</p></div>
                        </div>
                        <div class="row justify-content-between">
                            <div class="col"><a href="metier/Recherche.php"><p class="texte">Recherche</p></a></div>
                            <div class="col"><p class="texte">Marketing</p></div>
                            <div class="col"><p class="texte"></p></div>
                        </div>
                        <div class="row justify-content-between">
                            <div class="col"><a href="metier/organisme-de-formation.php"><p class="texte">Organisme</p></a></div>
                            <div class="col"><p class="texte"></p></div>
                            <div class="col"><p class="texte"></p></div>
                        </div>
                    </div>
                    <div class="col-lg-3 PR m-auto">
                        <table>
                            <tr><td><a href="https://www.facebook.com/treco.tech/" target="_blank"><img src="images/fb.png" width="24px"/></a></td></tr>
                            <tr><td><a href="https://fr.linkedin.com/company/treco-tech" target="_blank"><img src="images/lkd.png" width="24px"/></a></td></tr>
                            <tr><td><a href="https://twitter.com/treco_tech" target="_blank"><img src="images/tw.jpg" width="24px"/></a></td></tr>
                            <tr><td><a href="https://treco.tech" target="_blank"><img src="images/Bl.png" width="24px"/></a></td></tr>
                        </table>
                        <img id="img_footer" src="images/TRECO-white.png" width="200px" height="65px"/>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="copyright">Copyright © 2018. Tous droits réservés.</div>
                </div>
            </div>
        </footer>
</html>			