<?php 
    session_start();
    include("Connexion.php");
    $_SESSION['page_actuelle']="verification_formation.php"; 
    $_SESSION['messageErreur']=null;
    ?>

<?php 
        function traiterChaine($chaine) { 
                $chaine = str_replace('\r\n', '<br/>', $chaine); 
                $chaine = str_replace('\r', '<br />', $chaine); 
                $chaine = str_replace('\n', '<br />', $chaine); 
                return $chaine; 
        }
	$retour_annonce = false;
	$erreur = false;
	if(!isset($_POST['titre']) || $_POST['titre']==null){
		$_SESSION['messageErreur']="Titre manquant <br />";
		$retour_annonce = true;
	}
	else{
		$titre = trim($bdd->quote($_POST['titre']),"'");
                $titre = traiterChaine($titre);
	}
	if(!isset($_POST['presentation']) || $_POST['presentation']==null){
		$_SESSION['messageErreur']=$_SESSION['messageErreur'].'Pensez à présentez votre annonce pour plus de cliques :p<br />';
		$retour_annonce = true;
	}
	else{
		$presentation = trim($bdd->quote($_POST['presentation']),"'");
                $presentation = traiterChaine($presentation);
	}
        if(isset($_POST['lien']) || $_POST['lien']!=null){
		$lien = trim($bdd->quote($_POST['lien']),"'");
                $lien = traiterChaine($lien);
                $presentation .= '<br /><a href="'.$lien.'">Site de l\'organisme</a>';
	}
	if(!isset($_POST['objectifs']) || $_POST['objectifs']==null){
		$_SESSION['messageErreur']=$_SESSION['messageErreur'].'A quoi peut bien servir cette annonce ? <br />';
		$retour_annonce = true;
	}
	else{
		$objectifs = trim($bdd->quote($_POST['objectifs']),"'");
                $objectifs = traiterChaine($objectifs);
	}
	if(!isset($_POST['programme']) || $_POST['programme']==null){
		//$_SESSION['messageErreur']=$_SESSION['messageErreur'].'Il manque le programme ! <br />';
		$retour_annonce = true;
		$programme = "non obligatoire";
	}
	else{
		$programme = trim($bdd->quote($_POST['programme']),"'");
                $programme = traiterChaine($programme);
	}
	if(!isset($_POST['prerequis']) || $_POST['prerequis']==null){
		$_SESSION['messageErreur']=$_SESSION['messageErreur'].'De quel niveau a-t-on besoin ? <br />';
		$retour_annonce = true;
	}
	else{
		$prerequis = trim($bdd->quote($_POST['prerequis']),"'");
                $prerequis = traiterChaine($prerequis);
	}
	if(!isset($_POST['public']) || $_POST['public']==null){
		$_SESSION['messageErreur']=$_SESSION['messageErreur'].'Interdis aux enfants non ? <br />';
		$retour_annonce = true;
	}
	else{
		$public = trim($bdd->quote($_POST['public']),"'");
                $public = traiterChaine($public);
	}
	if(!isset($_POST['domaine_1']) || $_POST['domaine_1']==null){
		$_SESSION['messageErreur']=$_SESSION['messageErreur'].'Il manque le domaine ^^\' <br />';
		$retour_annonce = true;
	}
	else{
		$domaine_1 = trim($bdd->quote($_POST['domaine_1']),"'");
                $domaine_1 = traiterChaine($domaine_1);
	}
	if(!isset($_POST['domaine_2']) || $_POST['domaine_2']==null){
		$_SESSION['messageErreur']=$_SESSION['messageErreur'].'Il manque le domaine secondaire ^^\' <br />';
		$retour_annonce = true;
	}
	else{
		$domaine_2 = trim($bdd->quote($_POST['domaine_2']),"'");
                $domaine_2 = traiterChaine($domaine_2);
	}
	if(!isset($_POST['modalite']) || $_POST['modalite']==null){
		$_SESSION['messageErreur']=$_SESSION['messageErreur'].'Et vous ? où aimeriez-vous apprendre ? <br />';
		$retour_annonce = true;
	}else{
		$modalite = trim($bdd->quote($_POST['modalite']),"'");
                $modalite = traiterChaine($modalite);
	}
        if(!isset($_POST['duree']) || $_POST['duree']==null){
		$_SESSION['messageErreur']=$_SESSION['messageErreur'].'Combien de temps doit-elle durée ? <br />';
		$retour_annonce = true;
	}else{
		$duree = trim($bdd->quote($_POST['duree']),"'");
                $duree = traiterChaine($duree);
	}
	
	if($retour_annonce==true && isset($_GET['formulaire'])) header('Location: ajouter-formation.php?formulaire='.$_GET['formulaire'].'&amp;modification=true');
	elseif($retour_annonce && !isset($_GET['formulaire'])){header('Location: ajouter-formation.php');}
	else{
		/*
		 * Vérifier si les 3 domaines non remplis peuvent être null et faire entrer la formation dans la bdd
		 */
		if(!isset($_POST['HT']) || $_POST['HT']==null){
			$ht = null;
		}
		else{
			$ht = trim($bdd->quote($_POST['HT']),"'");
		}
		if(!isset($_POST['TTC']) || $_POST['TTC']==null){
			$ttc = null;
		}
		else{
			$ttc = trim($bdd->quote($_POST['TTC']),"'");
		}
		if(!isset($_POST['adresse']) || $_POST['adresse']==null){
			$adresse = null;
		}
		else{
		    $id_tv = 8;

		if (!isset($_GET['formulaire'])) {
                   $adresse = trim($bdd->quote($_POST['adresse']), "'");
                   $adresse = traiterChaine($adresse);

                //On ajoute une adresse dans la bdd
                try {
                    $requete_adresse = $bdd->prepare('INSERT INTO t_adresse(ID_TV,INDICATIF_PAYS,NOM_ADR,CODE_POSTAL,NUMERO) VALUES (:id_tv, :indicatif_pays, :nom_adr, :code_postal, :numero)');
                    $requete_adresse->execute(array(
                        'id_tv' => $id_tv,
                        'indicatif_pays' => 'FRA',
                        'nom_adr' => $adresse,
                        'code_postal' => '76000',
                        'numero' => '352'
                    ));

                    $id_adresse = $bdd->lastInsertId();
                } catch (Exception $e) {
                    $erreur = true;
                }
            } else {
                $adresse = trim($bdd->quote($_POST['adresse']), "'");

                //On modifie une adresse dans la bdd
                try {
                    $requete_id_adresse = $bdd->query('SELECT * FROM t_formation WHERE ID_FORM='.$_GET['formulaire']);
                    $id_adr = $requete_id_adresse->fetch();
                    $requete_adresse = $bdd->prepare('UPDATE t_adresse SET ID_TV='.$id_tv.', NOM_ADR='.$adresse.' WHERE ID_ADR='.$id_adr['ID_ADR']);
                    $requete_adresse->execute();

                } catch (Exception $e) {
                    $erreur = true;
                }
            }
		}

		//informations manquantes au formulaire
		$video=null;
		//mettre un type de voix
        //mettre un indicatif_pays, un CP et un num
		
	}
       
	//Si tout est correct, on ajoute dans la base de données : 
	//On crée dans un premier temps une description qui sera réutilisée pour les recherches des utilisateurs

    if(!isset($_GET['formulaire'])){
        try {
            $requeteSql_description = $bdd->prepare('INSERT INTO t_description(CHAPEAU,PRE_REQUI,OBJECTIF,PUBLIC_CIBLE,VIDEO) VALUES (:chapeau, :prerequis, :objectif, :public_cible, :video)');
            $requeteSql_description->execute(array(
                'chapeau' => $presentation,
                'prerequis' => $prerequis,
                'objectif' => $objectifs,
                'public_cible' => $public,
                'video' => null
            ));

            $id_description = $bdd->lastInsertId();
        }catch(Exception $e){
            $erreur = true;
        }
    } else {
        try {
            $requete_id_description = $bdd->query('SELECT * FROM t_formation WHERE ID_FORM='.$_GET['formulaire']);
            $donnees_description = $requete_id_description->fetch();
            $requeteSql_description = $bdd->prepare('UPDATE t_description SET CHAPEAU='.$presentation.', PRE_REQUI='.$prerequis.', OBJECTIF='.$objectifs.', PUBLIC_CIBLE='.$public.'WHERE ID_DESCRIPTION='.$donnees_description['ID_DESCRIPTION']);
            $requeteSql_description->execute();

        }catch(Exception $e){
            $erreur = true;
        }
    }
    
    try{
       $requeteID_domaine2 = $bdd->query('SELECT * FROM at_domaine2 WHERE NOM_D2 LIKE "%'.$domaine_2.'%"');
       $donnees_domaine2 = $requeteID_domaine2->fetch();
    }catch(mysqli_sql_exception $e){
        echo $e;
    }

	//Identifiant de la modalité
	$requeteID_modalite = $bdd->query('SELECT * FROM at_modalite WHERE NOM_MOD LIKE "%'.$modalite.'%"');
	$donnees_modalite = $requeteID_modalite->fetch();
    $requeteID_modalite->closeCursor();

    if(!isset($_GET['formulaire'])){
        try {
            $requeteSql_formation = $bdd->prepare('INSERT INTO t_formation(ID_ORGA,ID_D2,ID_ADR,ID_MOD,ID_DESCRIPTION,TITRE,PRIX,DUREE,LIEN,RECONNU,PROMO,REDUCTION) VALUES (:id_orga,:id_d2, :id_adr, :id_mod, :id_desc, :titre, :prix, :duree, :lien, :reconnu, :promo, :reduction)');
            $requeteSql_formation->execute(array(
                'id_orga'=> 1,
                'id_d2' => $donnees_domaine2['ID_D2'],
                'id_adr' => $id_adresse,
                'id_mod' => $donnees_modalite['ID_MOD'],
                'id_desc' => $id_description,
                'titre' => $titre,
                'prix' => $ttc,
                'duree' => $duree,
                'lien' => $lien,
                'reconnu' => 0,
                'promo' => 0,
                'reduction' => 0
            ));

        }catch (mysqli_sql_exception $e)
        {
            $erreur = true;
            $_SESSION['messageErreur'] = $e;
        }
    } else {
        try {
            $requeteSql_adresse = $bdd->query('SELECT * FROM t_formation WHERE ID_FORM='.$_GET['formulaire']);
            $donnees_formation = $requeteSql_adresse->fetch();
            $requeteSql_formation = $bdd->prepare('UPDATE t_formation SET ID_ORGA=1/*.$_SESSION[\'id\'].*/, ID_D2='.$donnees_domaine2['ID_D2'].', ID_ADR='.$donnees_formation['ID_ADR'].', ID_MOD='.$donnees_modalite['ID_MOD'].', ID_DESCRIPTION='.$donnees_formation['ID_DESCRIPTION'].', TITRE='.$titre.', PRIX='.$ttc.', DUREE='.$duree.', LIEN='.$lien.', RECONNU=1, PROMO=0, REDUCTION=0 WHERE ID_FORL='.$_GET['formulaire']);
            $requeteSql_formation->execute();

        }catch (mysqli_sql_exception $e)
        {
            $erreur = true;
            $_SESSION['messageErreur'] = $e;
        }
    }

    if($erreur){
       header('Location: ajouter-formation.php');
    }
    if($erreur && !isset($_GET['formulaire'])){
	    header('Location: ajouter-formation.php');
    } else { header('Location: ajouter-formation.php?formulaire='.$_GET['formulaire'].'&amp;modification=true');}
    header('Location:../index.php');
?>				