<?php
/**
 * Created by PhpStorm.
 * User: Delaunay Corentin
 * Date: 30/05/2018
 * Time: 17:42
 */
    session_start();
    $_SESSION['message']=null;
    $erreur=false;

    echo '<!DOCTYPE html>
          <html>
          <head>
          <link href="../style/envoie_mail.css" rel="stylesheet"/>
          </head><body></body></html>';

    if($_SESSION['page_actuelle']=='fiche-formation.php' || $_SESSION['page_actuelle']=='organisme-de-formation.php') {
        // Récupération des variables et sécurisation des données
        if (!isset($_POST['nom']) || $_POST['nom'] == null) {
            $_SESSION['message'] = "<p>un ou des champs sont manquants</p>";
            $erreur = true;
        } else
            $nom = htmlentities($_POST['nom']); // htmlentities() convertit des caractères "spéciaux" en équivalent HTML

        if (!isset($_POST['prenom']) || $_POST['prenom'] == null) {
            $_SESSION['message'] = "<p>un ou des champs sont manquants</p>";
            $erreur = true;
        } else
            $prenom = htmlentities($_POST['prenom']);

        if (!isset($_POST['mail']) || $_POST['mail'] == null) {
            $_SESSION['message'] = "<p>un ou des champs sont manquants</p>";
            $erreur = true;
        } else
            $email = htmlentities($_POST['mail']);

        if (!isset($_POST['telephone']) || $_POST['telephone'] == null) {
            $_SESSION['message'] = "<p>un ou des champs sont manquants</p>";
            $erreur = true;
        } else
            $telephone = htmlentities($_POST['telephone']);

        if ($erreur)
            header('Location: '.$_SESSION['page_actuelle']);

    }else{
        // Récupération des variables et sécurisation des données
        if (!isset($_POST['nom']) || $_POST['nom'] == null) {
            $_SESSION['message'] = "<p>un ou des champs sont manquants</p>";
            $erreur = true;
        } else
            $nom = htmlentities($_POST['nom']); // htmlentities() convertit des caractères "spéciaux" en équivalent HTML

        if (!isset($_POST['prenom']) || $_POST['prenom'] == null) {
            $_SESSION['message'] = "<p>un ou des champs sont manquants</p>";
            $erreur = true;
        } else
            $prenom = htmlentities($_POST['prenom']);

        if (!isset($_POST['email']) || $_POST['email'] == null) {
            $_SESSION['message'] = "<p>un ou des champs sont manquants</p>";
            $erreur = true;
        } else
            $email = htmlentities($_POST['email']);

        if (!isset($_POST['message']) || $_POST['message'] == null) {
            $_SESSION['message'] = "<p>un ou des champs sont manquants</p>";
            $erreur = true;
        } else
            $message = htmlentities($_POST['message']);

        if ($erreur)
            header('Location: '.$_SESSION['page_actuelle']);
    }
    // Variables concernant l'email
    $destinataire = 'contact@mytreco.fr'; // Adresse email du webmaster (à personnaliser)
    if($_SESSION['page_actuelle']=='fiche-formation.php')
        $sujet = 'Recherche de formation';
    else $sujet = $nom.'-'.$prenom;
    $contenu = '<html><head><title>'.$sujet.'</title></head><body>';
    $contenu .= '<p>Bonjour, vous avez reçu un message à partir de votre site web.</p>';
    $contenu .= '<p><strong>Nom :</strong> '.$nom.'</p>';
    $contenu .= '<p><strong>Prenom :</strong> '.$prenom.'</p>';
    $contenu .= '<p><strong>Email :</strong> '.$email.'</p>';
    if(isset($message)) $contenu .= '<p><strong>Message :</strong> '.$message.'</p>';
    else $contenu .= '<p><strong>Téléphone :</strong> '.$telephone.'</p>';
    $contenu .= '<p><strong>Nom de la page : </strong>'.$_SESSION['page_actuelle'].'</p>';
    $contenu .= '</body></html>'; // Contenu du message de l'email (en XHTML)

    // Pour envoyer un email HTML, l'en-tête Content-type doit être défini
    $headers = 'MIME-Version: 1.0'."\r\n";
    $headers .= 'Content-type: text/html; charset=UTF-8'."\r\n";

    // Envoyer l'email
    mail($destinataire, $sujet, $contenu, $headers); // Fonction principale qui envoi l'email
    $_SESSION['message']="message envoyé";
    //echo '<h2>Message envoyé!</h2>'; // Afficher un message pour indiquer que le message a été envoyé
    // (2) Fin du code pour traiter l'envoi de l'email

 /*Begin MailChimp Signup Form*/
    ?>

    <div id="mc_embed_signup">
        <form action="https://mytreco.us18.list-manage.com/subscribe/post?u=9e10959501173a5418fdab3cd&amp;id=9eb2a6c570" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
            <div id="mc_embed_signup_scroll">
                <div class="indicates-required"><span class="asterisk">*</span> indicates required</div>
                <div class="form-group">
                    <label for="mce-EMAIL" class="col-form-label">contact@mytreco.fr  <span class="asterisk">*</span>
                    </label>
                    <input type="email" value="<?php echo "$email"; ?>" name="EMAIL" class="required email form-control" id="mce-EMAIL">
                </div>
                <div class="form-group">
                    <label for="mce-FNAME" class="col-form-label">First Name </label>
                    <input type="text" value="<?php echo "$prenom"; ?>" name="FNAME" class="form-control" id="mce-FNAME">
                </div>
                <div class="form-group">
                    <label for="mce-LNAME" class="col-form-label">Last Name </label>
                    <input type="text" value="<?php echo "$nom"; ?>" name="LNAME" class="form-control" id="mce-LNAME">
                </div>
                <div class="form-group">
                    <label for="mce-PHONE" class="col-form-label">Phone </label>
                    <input type="text" value="<?php echo "$telephone"; ?>" name="PHONE" class="form-control" id="mce-PHONE">
                </div>
                <div id="mce-responses" class="clear">
                    <div class="response" id="mce-error-response" style="display:none"></div>
                    <div class="response" id="mce-success-response" style="display:none"></div>
                </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_9e10959501173a5418fdab3cd_9eb2a6c570" tabindex="-1" value=""/></div>
                <div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button btn btn-primary"></div>
            </div>
        </form>
    </div>
    <script type='text/javascript' src="//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js"></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';fnames[3]='ADDRESS';ftypes[3]='address';fnames[4]='PHONE';ftypes[4]='phone';}(jQuery));var $mcj = jQuery.noConflict(true);</script>
    <script type="text/javascript">
        document.getElementById("mc-embedded-subscribe").click();
        document.location.href = "<?php $_SESSION['page_actuelle'] ?>";
    </script>
	