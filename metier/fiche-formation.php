<?php
    session_start();
    include("Connexion.php");
    if(!isset($_SESSION['etat'])){$_SESSION['etat']="deconnecte";}
    if(!isset($_SESSION['type'])){$_SESSION['type']=null;}
    if(!isset($_SESSION['id'])){$_SESSION['id']=null;}
    if(isset($_GET['numero'])){
         $_SESSION['numero'] = $_GET['numero'];
         $requeteSql_formation = $bdd->query('SELECT * FROM t_formation WHERE ID_FORM=' . $_GET['numero']);
         $donnees_formation = $requeteSql_formation->fetch();
         $title = $donnees_formation['TITRE'];
         $_SESSION['title'] = $title;
    }else header('Location:Recherche.php');
    $_SESSION['page_actuelle']="fiche-formation.php?numero=".$_GET['numero']."&nom=".$_SESSION['title'];
    ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width" />
        <link href="../style/bootstrap.css" rel="stylesheet"/>
        <link href="../style/fiche-formation.css" rel="stylesheet" />
        <link rel="shortcut icon" type="image/x-icon" href="../images/favicon.ico" />
        <?php echo '<title>TRECO, fiche '.$title.' </title>';?>
            <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-119675621-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-119675621-1');
        </script>
        <script type="text/javascript">
            $('#exampleModal').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget) // Button that triggered the modal
                var recipient = button.data('whatever') // Extract info from data-* attributes
                // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
                // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
                var modal = $(this)
                modal.find('.modal-title').text('New message to ' + recipient)
                modal.find('.modal-body input').val(recipient)
            })
        </script>
        <script type="text/javascript">
            $('#ModalLong').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget) // Button that triggered the modal
                var recipient = button.data('whatever') // Extract info from data-* attributes
                // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
                // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
                var modal = $(this)
                modal.find('.modal-title').text('New message to ' + recipient)
                modal.find('.modal-body input').val(recipient)
            })
        </script>
        <script>
            // Example starter JavaScript for disabling form submissions if there are invalid fields
            (function() {
                'use strict';
                window.addEventListener('load', function() {
                    // Fetch all the forms we want to apply custom Bootstrap validation styles to
                    var forms = document.getElementsByClassName('needs-validation');
                    // Loop over them and prevent submission
                    var validation = Array.prototype.filter.call(forms, function(form) {
                        form.addEventListener('submit', function(event) {
                            if (form.checkValidity() === false) {
                                event.preventDefault();
                                event.stopPropagation();
                            }
                            form.classList.add('was-validated');
                        }, false);
                    });
                }, false);
            })();
        </script>
        <?php
            if(isset($_SESSION['message']))
                echo'<script>alert('.$_SESSION['message'].')</script>';
        ?>
        <script type="text/javascript"> _linkedin_data_partner_id = "374204"; </script><script type="text/javascript"> (function(){var s = document.getElementsByTagName("script")[0]; var b = document.createElement("script"); b.type = "text/javascript";b.async = true; b.src = "https://snap.licdn.com/li.lms-analytics/insight.min.js"; s.parentNode.insertBefore(b, s);})(); </script> <noscript> <img height="1" width="1" style="display:none;" alt="" src="https://dc.ads.linkedin.com/collect/?pid=374204&fmt=gif
https://dc.ads.linkedin.com/collect/?pid=374204&fmt=gif
" /> </noscript>
    </head>

    <header class="site-header">
        <div class="container">
            <div class="row" >
                <div class="col-lg-3">
                    <a href="../index.php"><img id="img_tete" src="../images/TRECO-white.png" width="60%"/></a>
                </div>
                <div class="row col-lg-6 mx-auto rechercher">
                    <form method="get" action="Recherche.php" class="recherche">
                        <div class="row justify-content-between" width="100%">
                            <div class="col-5">
                                <input id="fonctionR" type="text" name="name" placeholder="Entrez votre recherche" />
                            </div>
                            <div class="col-1">
                                <button type="submit" class="btn btn-primary button-R"><i class="fas fa-search"></i></button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="row col-lg-3 mx-auto navigation">
                    <div class="col-lg-1 my-auto ">
                        <a id="menu" href="../index.php">Accueil</a>
                    </div>
                    <div class="col-lg-1 my-auto mx-auto">
                        <a id="menu" href="organisme-de-formation.php">Organisme</a>
                    </div>
                    <div class="col-lg-1 my-auto">
                        <a id="menu" href="Contacts.php">Contacts</a>
                    </div>
                </div>
            </div>
        </div>
    </header>
    
    <body>
        <div id="hero-banner">
            <div class="container">
                <div class="row">
                    <div class="col-8" id="titre">
                        <div class="row r1">
                            <h1><?php
                                if(isset($_GET['numero'])) {
                                    $requeteSql_formation = $bdd->query('SELECT * FROM t_formation WHERE ID_FORM=' . $_GET['numero']);
                                    $donnees_formation = $requeteSql_formation->fetch();
                                    echo strtoupper($donnees_formation['TITRE']);
                                    $title = $donnees_formation['TITRE'];
                                }else{header('Location: Recherche.php');}
                                ?>
                            </h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
            
        <section id="programme">
                <div class="container txt">
                    <div class="row">
                        <div class="col">
                    <?php
                        if(isset($_GET['numero'])){
                            $requeteSql_formation = $bdd->query('SELECT * FROM t_formation WHERE ID_FORM='.$_GET['numero']);
                            $donnees_formation = $requeteSql_formation->fetch();
                            $requeteSql_description = $bdd->query('SELECT * FROM t_description WHERE ID_DESCRIPTION='.$donnees_formation['ID_DESCRIPTION']);
                            $donnees_description = $requeteSql_description->fetch();
                            /*$video = $donnees_description['VIDEO'];
                            if($video!=null && $video!=""){
                                echo '<div class="content" id="video" style="visibility: hidden;">';
                                echo '<video width="60%" heigth="60%" autoplay="true" loop >';
                                echo '<source src="'.$video.'" type="video/mp4"/>';
                                echo '</video>';
                                echo '</div>';
                            }*/
                            $programme = $donnees_description['Programme'];
                            if($programme!=null || $programme!=""){
                                echo '<div class="row desc">';
                                echo '<div class="col">';
                                echo '<h3>Programme</h3>';
                                echo '<p>'.$programme.'</p>';
                                echo '</div></div>';
                            }
                            $prerequis = $donnees_description['PRE_REQUI'];
                            if($prerequis!=null || $prerequis!=""){
                                echo '<div class="row prerequis">';
                                echo '<div class="col">';
                                echo '<h3>Prérequis</h3>';
                                echo '<p>'.$prerequis.'</p>';
                                echo '</div></div>';
                            }
                            $public_cible = $donnees_description['PUBLIC_CIBLE'];
                            if($public_cible!= null || $public_cible!=""){
                                echo '<div class="row public">';
                                echo '<div class="col">';
                                echo '<h3>Public visé</h3>';
                                echo '<p>'.$public_cible.'</p>';
                                echo '</div></div>';
                            }

                            echo '<div class="row commentaires" >';
                            echo '<div class="col">';
                            echo '<h3>Commentaires</h3>';
                            /*echo '    <p id="pseudo" >  Frédérique : </p><p class="trait_dessus"></p>';
                            echo '    <span>Evaluation :    </span>';
                            echo '    <span class="progress"><progress id="avancement" class="progressBar" value="4" max="5"></progress>';
                            echo '    </span><div class="progressValue">80%</div>';*/
                            echo '</div></div>';
                        }
                    ?>
                        </div>
                        <div class="col plus">
                            <div class="aside" id="lateral">
                                <ul>
                                    <?php
                                    $requeteSql_organisme = $bdd->query('SELECT * FROM t_organisation WHERE ID_ORGA =' . $donnees_formation['ID_ORGA']);
                                    $donnees_organisme = $requeteSql_organisme->fetch();
                                    echo '<li>Organisme : ';
                                    echo $donnees_organisme['NOM_ORGA'] .'</li>';
                                    echo '<div class="trait_dessus"><hr></div>';
                                    echo '<li>Durée : ';
                                    echo $donnees_formation['DUREE'].' heures</li>';
                                    echo '<div class="trait_dessus"><hr></div>';
                                    echo '<li>Modalité pédagogique : ';
                                    $requeteSql_mod = $bdd->query('SELECT * FROM at_modalite WHERE ID_MOD=' . $donnees_formation['ID_MOD']);
                                    $donnees_mod = $requeteSql_mod->fetch();
                                    echo $donnees_mod['NOM_MOD'].'</li>';
                                    echo '<div class="trait_dessus"><hr></div>';
                                    echo '<li>Prix :';
                                    if($donnees_formation['PRIX']==0){
                                        echo ' - €</li>';
                                    }else{
                                        echo $donnees_formation['PRIX'].'€ </li>';
                                    }
                                    echo '<div class="trait_dessus"><hr></div>';
                                    ?>
                                </ul>
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" data-whatever="@getbootstrap">Cette formation vous intéresse ?</button>

                                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Nouveau message</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <form class="needs-validation" method="post" action="envoie_mail.php" novalidate>
                                                    <div class="form-group">
                                                        <label for="nom validationCustom01" class="col-form-label">Nom</label>
                                                        <input type="text" class="form-control" name="nom" id="nom validationCustom01" placeholder="Nom" required>
                                                        <div class="valid-feedback">
                                                            Looks good!
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="prenom validationCustom02" class="col-form-label">Prenom</label>
                                                        <input type="text" class="form-control" name="prenom" id="prenom validationCustom02" placeholder="Prenom" required>
                                                        <div class="valid-feedback">
                                                            Looks good!
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="mail validationCustom04" class="col-form-label">E-mail</label>
                                                        <input type="text" class="form-control" name="mail"id="mail validationCustom04" placeholder="E-mail" required>
                                                        <div class="valid-feedback">
                                                            Looks good!
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="telephone validationCustom03" class="col-form-label">Téléphone</label>
                                                        <input type="text" class="form-control" name="telephone" id="Telephone validationCustom03" placeholder="Téléphone" required>
                                                        <div class="valid-feedback">
                                                            Looks good!
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="checkbox" value="" id="invalidCheck" required>
                                                            <label class="form-check-label" for="invalidCheck">

                                                                <button type="button" class="boutonC" data-toggle="modal" data-target="#ModalLong">
                                                                    Agree to terms and conditions
                                                                </button>

                                                                <div class="modal fade" id="ModalLong" tabindex="-1" role="dialog" aria-labelledby="ModalLongTitle" aria-hidden="true">
                                                                    <div class="modal-dialog" role="document">
                                                                        <div class="modal-content">
                                                                            <div class="modal-header">
                                                                                <h5 class="modal-title" id="ModalLongTitle">Conditions générales d'utilisation</h5>
                                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                    <span aria-hidden="true">&times;</span>
                                                                                </button>
                                                                            </div>
                                                                            <div class="modal-body">
                                                                                <div class=WordSection1>

                                                                                    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
text-align:justify;line-height:normal;mso-outline-level:1'><b><span
                                                                                                    style='font-size:24.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black;mso-font-kerning:18.0pt;mso-fareast-language:
FR'>Conditions générales d'utilisation du site internet TRECO et notifications<p></p></span></b></p>

                                                                                    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
text-align:justify;line-height:normal'><span style='font-size:13.5pt;
font-family:"Arial",sans-serif;mso-fareast-font-family:"Times New Roman";
color:black;mso-fareast-language:FR'>Bienvenue sur le site internet TRECO ou
sur les applications mobiles, y compris les applications connexes
(conjointement dénommés le «<b>&nbsp;Site&nbsp;</b>»). Ce Site est fourni
uniquement pour aider les clients à rassembler des informations relatives aux formations
professionnelles, à soumettre des avis sur des questions relatives aux formations
professionnelles, à chercher et réserver des formations professionnelles,
exclusivement. Les termes «<b>&nbsp;nous&nbsp;</b>», «<b>&nbsp;notre&nbsp;</b>»,
«<b>&nbsp;nos&nbsp;</b>» et «<b>&nbsp;TRECO&nbsp;</b>» font référence à TRECO,
à nos affiliés et à notre site internet (dénommé «&nbsp;TRECO&nbsp;»). Le terme
«&nbsp;vous&nbsp;» fait référence au client qui visite le Site et/ou qui
alimente le contenu de ce Site.<p></p></span></p>

                                                                                    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
text-align:justify;line-height:normal'><span style='font-size:13.5pt;
font-family:"Arial",sans-serif;mso-fareast-font-family:"Times New Roman";
color:black;mso-fareast-language:FR'>Ce Site vous est proposé sous réserve de
votre acceptation, sans modification, des conditions générales et notifications
exposées ci-dessous (conjointement dénommées l' «&nbsp;Accord&nbsp;»). En
accédant à ce Site ou en utilisant ce Site de quelque façon que ce soit, vous
acceptez d’être lié par cet Accord et vous déclarez avoir lu et compris ses
dispositions. Prière de lire attentivement cet Accord. Il contient des
informations relatives à vos droits juridiques et aux restrictions imposées à
ces droits, de même qu'une section consacrée à la législation en vigueur et au
règlement des contentieux. Si vous n'acceptez pas ces conditions d'utilisation,
vous n'êtes pas autorisé à utiliser ce Site.<p></p></span></p>

                                                                                    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
text-align:justify;line-height:normal'><span style='font-size:13.5pt;
font-family:"Arial",sans-serif;mso-fareast-font-family:"Times New Roman";
color:black;mso-fareast-language:FR'>Nous nous réservons le droit de changer ou
de modifier l'Accord, conformément à ces conditions générales, et vous
comprenez et acceptez que, en continuant d'utiliser ce Site ou d'y accéder,
vous acceptez toutes les modifications qui y sont apportées. La date des
dernières modifications de l'Accord figurera au bas de cette page et toute
modification prendra effet après sa publication. Nous informerons nos membres
des modifications matérielles apportées à ces conditions générales
d'utilisation soit en leur envoyant une notification à l'adresse électronique
qu'ils nous ont fournie lors de leur enregistrement soit en signalant les
modifications sur notre Site. Assurez-vous de retourner régulièrement sur cette
page pour prendre connaissance de la dernière version de l'Accord.<p></p></span></p>

                                                                                    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
text-align:justify;line-height:normal;mso-outline-level:4'><b><span
                                                                                                    style='font-size:13.5pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black;mso-fareast-language:FR'>UTILISATION DU SITE<p></p></span></b></p>

                                                                                    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
text-align:justify;line-height:normal'><span style='font-size:13.5pt;
font-family:"Arial",sans-serif;mso-fareast-font-family:"Times New Roman";
color:black;mso-fareast-language:FR'>L'utilisation de ce Site suppose que vous
garantissiez que (i) toutes les informations que vous fournissez sur ce Site
sont vraies, précises, à jour et complètes, (ii) si vous possédez un compte TRECO,
vous protégerez les renseignements relatifs à votre compte et vous superviserez
et serez entièrement responsable de toute utilisation de votre compte par un
tiers, (iii) vous avez 13 ans ou plus, ce qui vous autorise à créer un compte
et à alimenter le contenu de notre Site et (iv) vous disposez de la capacité
juridique pour conclure le présent Accord et pour utiliser ce Site conformément
à ces conditions générales.<p></p></span></p>

                                                                                    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
text-align:justify;line-height:normal'><span style='font-size:13.5pt;
font-family:"Arial",sans-serif;mso-fareast-font-family:"Times New Roman";
color:black;mso-fareast-language:FR'>TRECO ne collecte sciemment aucune
information provenant d'enfants de moins de 13 ans. Nous nous réservons le
droit exclusif d’interdire l’accès à ce Site et aux services que nous offrons à
tout moment, et pour toute raison, notamment mais pas exclusivement, pour
violation de cet Accord.<p></p></span></p>

                                                                                    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
text-align:justify;line-height:normal'><span style='font-size:13.5pt;
font-family:"Arial",sans-serif;mso-fareast-font-family:"Times New Roman";
color:black;mso-fareast-language:FR'>Sont strictement interdits toute copie,
transmission, reproduction, <span class=SpellE>ré-édition</span>,
redistribution ou tout envoi du contenu du Site, en totalité ou en partie, sans
l'autorisation préalable et écrite de TRECO. Pour obtenir cette autorisation,
vous pouvez contacter TRECO à l'adresse suivante&nbsp;:<p></p></span></p>

                                                                                    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
text-align:justify;line-height:normal'><span style='font-size:13.5pt;
font-family:"Arial",sans-serif;mso-fareast-font-family:"Times New Roman";
color:black;mso-fareast-language:FR'>CEO Bertrand Jegou du Laz<p></p></span></p>

                                                                                    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
text-align:justify;line-height:normal'><span style='font-size:13.5pt;
font-family:"Arial",sans-serif;mso-fareast-font-family:"Times New Roman";
color:black;mso-fareast-language:FR'>TRECO<p></p></span></p>

                                                                                    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
text-align:justify;line-height:normal'><span style='font-size:13.5pt;
font-family:"Arial",sans-serif;mso-fareast-font-family:"Times New Roman";
color:black;mso-fareast-language:FR'>88b rue Thomas Dubosc <p></p></span></p>

                                                                                    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
text-align:justify;line-height:normal'><span style='font-size:13.5pt;
font-family:"Arial",sans-serif;mso-fareast-font-family:"Times New Roman";
color:black;mso-fareast-language:FR'>76000 Rouen, France<p></p></span></p>

                                                                                    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
text-align:justify;line-height:normal'><span style='font-size:13.5pt;
font-family:"Arial",sans-serif;mso-fareast-font-family:"Times New Roman";
color:black;mso-fareast-language:FR'>L'utilisation du Site conformément aux
présentes conditions générales est gratuite pour ses utilisateurs. Le Site
contient toutefois des liens vers des sites internet de tiers, lesquels sont
détenus et exploités par des revendeurs ou des prestataires de service
indépendants. Il se peut que ces tiers rendent payante l'utilisation de
certains contenus ou de certains services fournis sur le site internet. Avant
de poursuivre une transaction avec un tiers, il vous appartient donc
d'effectuer toutes les vérifications que vous jugerez nécessaires ou opportunes
afin de déterminer les frais encourus. Lorsque TRECO fournit sur le Site des
détails concernant des coûts, ces informations ne sont fournies qu'à titre
indicatif et par souci de commodité. En aucun cas TRECO ne garantit
l'exactitude de telles informations et, en aucun cas, TRECO ne saurait être
responsable du contenu ou des services fournis sur les sites internet des tiers
en question.<p></p></span></p>

                                                                                    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
text-align:justify;line-height:normal;mso-outline-level:4'><b><span
                                                                                                    style='font-size:13.5pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black;mso-fareast-language:FR'>ACTIVITÉS NON AUTORISÉES<p></p></span></b></p>

                                                                                    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
text-align:justify;line-height:normal'><span style='font-size:13.5pt;
font-family:"Arial",sans-serif;mso-fareast-font-family:"Times New Roman";
color:black;mso-fareast-language:FR'>Le contenu et les informations de ce Site
(incluant, mais sans s'y limiter, des messages, données, informations, textes,
musiques, sons, photos, graphiques, vidéos, cartes, icônes, logiciels, codes ou
autre élément), tout comme l'infrastructure utilisée pour fournir ces contenus
et ces informations nous appartiennent. Vous acceptez de ne pas modifier,
copier, distribuer, transmettre, afficher, mettre à disposition, reproduire,
publier, octroyer une licence, créer des œuvres dérivées, transférer, ou vendre
ou revendre toute information, tout logiciel, produit ou service obtenus depuis
ou grâce à ce Site. En outre, vous vous engagez à ne pas&nbsp;:<p></p></span></p>

                                                                                    <ul type=disc>
                                                                                        <li class=MsoNormal style='color:black;mso-margin-top-alt:auto;mso-margin-bottom-alt:
     auto;text-align:justify;line-height:normal;mso-list:l0 level1 lfo1;
     tab-stops:list 36.0pt'><span style='font-size:13.5pt;font-family:"Arial",sans-serif;
     mso-fareast-font-family:"Times New Roman";mso-fareast-language:FR'>(i)
     utiliser ce Site ou ses contenus à des fins commerciales&nbsp;;<p></p></span></li>
                                                                                        <li class=MsoNormal style='color:black;mso-margin-top-alt:auto;mso-margin-bottom-alt:
     auto;text-align:justify;line-height:normal;mso-list:l0 level1 lfo1;
     tab-stops:list 36.0pt'><span style='font-size:13.5pt;font-family:"Arial",sans-serif;
     mso-fareast-font-family:"Times New Roman";mso-fareast-language:FR'>(ii)
     utiliser, surveiller ou copier les contenus ou informations de ce Site à
     l'aide d'un robot, d'une araignée, d'un scraper ou de tout autre
     dispositif automatique ou procédé manuel, à quelque fin que ce soit, sans
     notre autorisation explicite et écrite&nbsp;;<p></p></span></li>
                                                                                        <li class=MsoNormal style='color:black;mso-margin-top-alt:auto;mso-margin-bottom-alt:
     auto;text-align:justify;line-height:normal;mso-list:l0 level1 lfo1;
     tab-stops:list 36.0pt'><span style='font-size:13.5pt;font-family:"Arial",sans-serif;
     mso-fareast-font-family:"Times New Roman";mso-fareast-language:FR'>(iii)
     violer les restrictions des fichiers d’exclusion des robots sur ce Site ni
     à contourner les mesures visant à empêcher ou limiter l’accès à ce
     Site&nbsp;;<p></p></span></li>
                                                                                        <li class=MsoNormal style='color:black;mso-margin-top-alt:auto;mso-margin-bottom-alt:
     auto;text-align:justify;line-height:normal;mso-list:l0 level1 lfo1;
     tab-stops:list 36.0pt'><span style='font-size:13.5pt;font-family:"Arial",sans-serif;
     mso-fareast-font-family:"Times New Roman";mso-fareast-language:FR'>(iv)
     prendre de mesure qui impose, ou pourrait imposer, à notre entière
     discrétion, une charge déraisonnable ou excessive à nos
     infrastructures&nbsp;;<p></p></span></li>
                                                                                        <li class=MsoNormal style='color:black;mso-margin-top-alt:auto;mso-margin-bottom-alt:
     auto;text-align:justify;line-height:normal;mso-list:l0 level1 lfo1;
     tab-stops:list 36.0pt'><span style='font-size:13.5pt;font-family:"Arial",sans-serif;
     mso-fareast-font-family:"Times New Roman";mso-fareast-language:FR'>(v)
     établir de lien invisible menant au Site, pour toute raison, sans notre
     autorisation explicite et écrite&nbsp;;<p></p></span></li>
                                                                                        <li class=MsoNormal style='color:black;mso-margin-top-alt:auto;mso-margin-bottom-alt:
     auto;text-align:justify;line-height:normal;mso-list:l0 level1 lfo1;
     tab-stops:list 36.0pt'><span style='font-size:13.5pt;font-family:"Arial",sans-serif;
     mso-fareast-font-family:"Times New Roman";mso-fareast-language:FR'>(vi)
     «&nbsp;encadrer&nbsp;», «&nbsp;refléter&nbsp;» ou incorporer toute partie
     de ce Site dans un autre site, sans notre autorisation explicite et
     écrite&nbsp;; ou<p></p></span></li>
                                                                                        <li class=MsoNormal style='color:black;mso-margin-top-alt:auto;mso-margin-bottom-alt:
     auto;text-align:justify;line-height:normal;mso-list:l0 level1 lfo1;
     tab-stops:list 36.0pt'><span style='font-size:13.5pt;font-family:"Arial",sans-serif;
     mso-fareast-font-family:"Times New Roman";mso-fareast-language:FR'>(vii)
     essayer de modifier, traduire, adapter, réviser, décompiler, désassembler,
     assembler à rebours tout programme logiciel utilisé par TRECO en lien avec
     le Site ou les services.<p></p></span></li>
                                                                                    </ul>

                                                                                    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
text-align:justify;line-height:normal;mso-outline-level:4'><b><span
                                                                                                    style='font-size:13.5pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black;mso-fareast-language:FR'>CHARTE DE
CONFIDENTIALITÉ<p></p></span></b></p>

                                                                                    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
text-align:justify;line-height:normal'><span style='font-size:13.5pt;
font-family:"Arial",sans-serif;mso-fareast-font-family:"Times New Roman";
color:black;mso-fareast-language:FR'>TRECO croit à la protection de la vie
privée. Les informations à caractère personnel que vous publierez sur le Site
seront utilisées conformément à notre Charte sur le respect de la vie privée.
Cliquez ici pour lire notre&nbsp;Charte sur le respect de la vie privée.<p></p></span></p>

                                                                                    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
text-align:justify;line-height:normal;mso-outline-level:4'><b><span
                                                                                                    style='font-size:13.5pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black;mso-fareast-language:FR'>AVIS, COMMENTAIRES ET
AUTRES ZONES INTERACTIVES<p></p></span></b></p>

                                                                                    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
text-align:justify;line-height:normal'><span style='font-size:13.5pt;
font-family:"Arial",sans-serif;mso-fareast-font-family:"Times New Roman";
color:black;mso-fareast-language:FR'>Nous sommes attentifs à vos remarques.
Veuillez noter qu’en soumettant des contenus à ce Site par courrier
électronique, publications sur le Site ou autre, y compris avis sur les formations
et organismes de formation, questions, photos, vidéos, commentaires,
suggestions, idées ou autres contenus dans toute contribution (conjointement
dénommés &quot;Contributions&quot;), vous concédez à TRECO le droit non
exclusif, exempt de redevance, perpétuel, transférable, irrévocable et
susceptible de faire l'objet d'une sous-licence de (a) utiliser, reproduire,
modifier, adapter, traduire, distribuer, publier, créer des œuvres dérivées,
afficher publiquement et mettre à disposition ces Contributions partout dans le
monde dans quelque média que ce soit, connu maintenant ou qui pourrait être mis
au point ultérieurement&nbsp;; et de (b) utiliser le nom que vous avez fourni
lors de la soumission de cette Contribution. Vous reconnaissez que TRECO peut
choisir d'utiliser votre nom à son entière discrétion. Vous autorisez également
TRECO à poursuivre en justice toute personne ou entité qui enfreint vos droits
ou ceux de TRECO par violation de cet Accord. Vous reconnaissez et convenez que
les Contributions sont non confidentielles et non privées.<p></p></span></p>

                                                                                    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
text-align:justify;line-height:normal'><span style='font-size:13.5pt;
font-family:"Arial",sans-serif;mso-fareast-font-family:"Times New Roman";
color:black;mso-fareast-language:FR'>TRECO ne revoit ni ne contrôle les
messages des utilisateurs publiés ou diffusés sur ce Site, notamment par le
biais de forums de discussion, de tableaux d'affichage ou d'autres forums de
communication, et ne sera en aucun cas tenu pour responsable des messages des
utilisateurs. TRECO se réserve néanmoins le droit de supprimer, à son entière
discrétion, sans préavis, et pour toute raison, tout message envoyé par les
utilisateurs et/ou contenu du Site.<p></p></span></p>

                                                                                    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
text-align:justify;line-height:normal'><span style='font-size:13.5pt;
font-family:"Arial",sans-serif;mso-fareast-font-family:"Times New Roman";
color:black;mso-fareast-language:FR'>Ce Site peut contenir des forums de discussion,
des tableaux d'affichage, des services d'évaluation ou d'autres forums à
travers lesquels vous ou des tiers pouvez publier sur ce Site des commentaires
sur vos formations professionnelles ou d'autres contenus, messages, matériels
ou éléments («&nbsp;Zones interactives&nbsp;»). Dans l'hypothèse où TRECO
fournirait de tels services, vous seriez seul responsable de la manière dont
vous utiliseriez ces Zones interactives et vous les utiliseriez à vos propres
risques. En utilisant des Zones interactives, vous vous engagez à ne publier,
télécharger, transmettre, diffuser, stocker, créer ou envoyer par le biais de
ce Site aucun des contenus suivants&nbsp;:<p></p></span></p>

                                                                                    <ul type=disc>
                                                                                        <li class=MsoNormal style='color:black;mso-margin-top-alt:auto;mso-margin-bottom-alt:
     auto;text-align:justify;line-height:normal;mso-list:l2 level1 lfo2;
     tab-stops:list 36.0pt'><span style='font-size:13.5pt;font-family:"Arial",sans-serif;
     mso-fareast-font-family:"Times New Roman";mso-fareast-language:FR'>Tout
     message, texte, son, graphique, code, toute donnée, information, musique,
     photographie ou tout autre élément («&nbsp;Contenu&nbsp;») faux, illégal,
     trompeur, diffamatoire, injurieux, obscène, pornographique, indécent,
     licencieux, suggestif, intimidant ou appelant au harcèlement d'une tierce
     personne, menaçant, irrespectueux des droits de la vie privée ou des
     droits de publication, abusifs, incendiaires, frauduleux ou
     répréhensibles&nbsp;;<p></p></span></li>
                                                                                        <li class=MsoNormal style='color:black;mso-margin-top-alt:auto;mso-margin-bottom-alt:
     auto;text-align:justify;line-height:normal;mso-list:l2 level1 lfo2;
     tab-stops:list 36.0pt'><span style='font-size:13.5pt;font-family:"Arial",sans-serif;
     mso-fareast-font-family:"Times New Roman";mso-fareast-language:FR'>Contenus
     manifestement injurieux pour la communauté des internautes, tels que des
     contenus incitant au racisme, au fanatisme, à la haine ou à toute attaque
     physique de groupes ou d'individus&nbsp;;<p></p></span></li>
                                                                                        <li class=MsoNormal style='color:black;mso-margin-top-alt:auto;mso-margin-bottom-alt:
     auto;text-align:justify;line-height:normal;mso-list:l2 level1 lfo2;
     tab-stops:list 36.0pt'><span style='font-size:13.5pt;font-family:"Arial",sans-serif;
     mso-fareast-font-family:"Times New Roman";mso-fareast-language:FR'>Contenus
     qui constituent, encouragent, promeuvent une activité illégale ou une
     infraction criminelle ou fournissent des indications sur la manière
     d'exécuter celles-ci, engagent la responsabilité civile, sont en
     infraction avec les droits d'un tiers dans tout autre pays du monde, ou
     qui engagent une responsabilité ou violent la législation locale,
     nationale ou internationale.<p></p></span></li>
                                                                                        <li class=MsoNormal style='color:black;mso-margin-top-alt:auto;mso-margin-bottom-alt:
     auto;text-align:justify;line-height:normal;mso-list:l2 level1 lfo2;
     tab-stops:list 36.0pt'><span style='font-size:13.5pt;font-family:"Arial",sans-serif;
     mso-fareast-font-family:"Times New Roman";mso-fareast-language:FR'>Contenus
     fournissant des instructions se rapportant à des activités illicites,
     telles que la fabrication ou l'achat d'armes illicites, l'atteinte à la
     vie privée d'un tiers, ou la création ou la fourniture de virus
     informatiques&nbsp;;<p></p></span></li>
                                                                                        <li class=MsoNormal style='color:black;mso-margin-top-alt:auto;mso-margin-bottom-alt:
     auto;text-align:justify;line-height:normal;mso-list:l2 level1 lfo2;
     tab-stops:list 36.0pt'><span style='font-size:13.5pt;font-family:"Arial",sans-serif;
     mso-fareast-font-family:"Times New Roman";mso-fareast-language:FR'>Contenus
     violant tout brevet, marque, secret de fabrication, droit d'auteur ou
     droit de propriété intellectuelle. En particulier, contenus promouvant la
     copie illicite ou non autorisée d'œuvres de tiers protégées par le droit
     d'auteur, par exemple en fournissant des programmes informatiques piratés
     ou de liens renvoyant à ces programmes, en diffusant des informations
     permettant de contourner les dispositifs de protection contre les copies
     installées par le fabricant, ou en fournissant de la musique piratée ou
     des liens renvoyant à des fichiers de musique piratée&nbsp;;<p></p></span></li>
                                                                                        <li class=MsoNormal style='color:black;mso-margin-top-alt:auto;mso-margin-bottom-alt:
     auto;text-align:justify;line-height:normal;mso-list:l2 level1 lfo2;
     tab-stops:list 36.0pt'><span style='font-size:13.5pt;font-family:"Arial",sans-serif;
     mso-fareast-font-family:"Times New Roman";mso-fareast-language:FR'>Contenus
     qui usurpent l'identité d'une personne ou d'une entité ou présentent de
     manière délibérément erronée vos rapports avec une personne ou une entité,
     y compris TRECO&nbsp;;<p></p></span></li>
                                                                                        <li class=MsoNormal style='color:black;mso-margin-top-alt:auto;mso-margin-bottom-alt:
     auto;text-align:justify;line-height:normal;mso-list:l2 level1 lfo2;
     tab-stops:list 36.0pt'><span style='font-size:13.5pt;font-family:"Arial",sans-serif;
     mso-fareast-font-family:"Times New Roman";mso-fareast-language:FR'>Promotions,
     publipostage ou «&nbsp;spamming&nbsp;», pourriels, chaînes de lettres,
     messages de campagnes politiques, publicités, concours, tirages au sort ou
     sollicitations indésirables&nbsp;;<p></p></span></li>
                                                                                        <li class=MsoNormal style='color:black;mso-margin-top-alt:auto;mso-margin-bottom-alt:
     auto;text-align:justify;line-height:normal;mso-list:l2 level1 lfo2;
     tab-stops:list 36.0pt'><span style='font-size:13.5pt;font-family:"Arial",sans-serif;
     mso-fareast-font-family:"Times New Roman";mso-fareast-language:FR'>Contenus
     comportant des activités commerciales et/ou de vente, sans notre
     autorisation préalable et écrite, tels que des concours, des loteries, du
     troc, de la publicité et des systèmes pyramidaux&nbsp;;<p></p></span></li>
                                                                                        <li class=MsoNormal style='color:black;mso-margin-top-alt:auto;mso-margin-bottom-alt:
     auto;text-align:justify;line-height:normal;mso-list:l2 level1 lfo2;
     tab-stops:list 36.0pt'><span style='font-size:13.5pt;font-family:"Arial",sans-serif;
     mso-fareast-font-family:"Times New Roman";mso-fareast-language:FR'>Informations
     à caractère confidentiel, y compris, sans restriction, nom de famille,
     adresses, numéros de téléphone, adresses électroniques, numéros de
     sécurité sociale et numéros de carte de crédit&nbsp;;<p></p></span></li>
                                                                                        <li class=MsoNormal style='color:black;mso-margin-top-alt:auto;mso-margin-bottom-alt:
     auto;text-align:justify;line-height:normal;mso-list:l2 level1 lfo2;
     tab-stops:list 36.0pt'><span style='font-size:13.5pt;font-family:"Arial",sans-serif;
     mso-fareast-font-family:"Times New Roman";mso-fareast-language:FR'>Contenant
     des pages à accès restreint ou dépendant d'un mot de passe, des pages ou
     des images cachées (qui ne proviennent pas d'autres pages accessibles ou
     qui n'y sont pas liées)&nbsp;;virus, données corrompues, ou fichiers
     nuisibles, destructeurs ou dommageables&nbsp;;<p></p></span></li>
                                                                                        <li class=MsoNormal style='color:black;mso-margin-top-alt:auto;mso-margin-bottom-alt:
     auto;text-align:justify;line-height:normal;mso-list:l2 level1 lfo2;
     tab-stops:list 36.0pt'><span style='font-size:13.5pt;font-family:"Arial",sans-serif;
     mso-fareast-font-family:"Times New Roman";mso-fareast-language:FR'>Contenus
     sans rapport avec le sujet de la Zone interactive sur laquelle ces
     contenus sont publiés&nbsp;; ou<p></p></span></li>
                                                                                        <li class=MsoNormal style='color:black;mso-margin-top-alt:auto;mso-margin-bottom-alt:
     auto;text-align:justify;line-height:normal;mso-list:l2 level1 lfo2;
     tab-stops:list 36.0pt'><span style='font-size:13.5pt;font-family:"Arial",sans-serif;
     mso-fareast-font-family:"Times New Roman";mso-fareast-language:FR'>Contenus
     ou liens menant à des contenus qui, selon TRECO, (a) constituent une
     violation des sous-sections précédentes, (b) sont répréhensibles, (c)
     empêchent toute autre personne d'utiliser les Zones interactives de ce
     Site ou d'en profiter, ou (d) qui peuvent exposer TRECO, ses affiliés ou
     ses utilisateurs à un risque ou engager leur responsabilité.<p></p></span></li>
                                                                                    </ul>

                                                                                    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
text-align:justify;line-height:normal'><span style='font-size:13.5pt;
font-family:"Arial",sans-serif;mso-fareast-font-family:"Times New Roman";
color:black;mso-fareast-language:FR'>TRECO décline toute responsabilité quant
au contenu publié, stocké ou téléchargé par vous ou par des tiers, quant aux
pertes ou aux dommages liés à ces derniers. De plus, TRECO n'est pas
responsable des erreurs, actes de diffamation, calomnie, omissions, mensonges,
contenus obscènes, pornographiques ou vulgaires que vous pourriez visualiser.
En tant que fournisseur de services interactifs, TRECO n'est pas responsable
des déclarations, représentations ou contenu fournis par ses utilisateurs dans
les forums publics, les pages d'accueil personnelles ou toute autre zone
interactive. Bien que TRECO ne soit pas tenu d'analyser, de modifier ou de
contrôler les contenus publiés dans une Zone interactive ou diffusés par le
biais de celle-ci, TRECO se réserve le droit de supprimer, d'analyser, de
traduire ou de modifier, à son entière discrétion et sans préavis, tout contenu
publié ou stocké sur ce Site, à tout moment et pour toute raison, ou de confier
ces tâches à des tiers, et vous seul êtes responsable de la création de copies
de sauvegarde et du remplacement à vos frais des contenus que vous publiez ou
stockez sur ce Site.<p></p></span></p>

                                                                                    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
text-align:justify;line-height:normal'><span style='font-size:13.5pt;
font-family:"Arial",sans-serif;mso-fareast-font-family:"Times New Roman";
color:black;mso-fareast-language:FR'>S'il est établi que vous conservez des
droits moraux sur le contenu (y compris droits d'attribution et droits à
l'intégrité), vous déclarez par la présente que (a) vous n'exigez pas que les
informations permettant d'identifier une personne soient utilisées en lien avec
le contenu, avec des œuvres dérivées, des mises à jour ou des améliorations de
celui-ci&nbsp;; (b) vous n'êtes pas opposé à la publication, l'utilisation, la
modification, la suppression et l'exploitation du contenu par TRECO, ses
titulaires de licence, successeurs et ayants droit&nbsp;; (c) vous vous engagez
à renoncer à tout droit moral d'auteur relatif au contenu&nbsp;et à ne jamais
les revendiquer&nbsp;; et (d) vous dégagez TRECO, ses titulaires de licence,
successeurs et ayants droit de toute responsabilité, que vous auriez pu faire
valoir en vertu de ces droits moraux.<p></p></span></p>

                                                                                    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
text-align:justify;line-height:normal'><span style='font-size:13.5pt;
font-family:"Arial",sans-serif;mso-fareast-font-family:"Times New Roman";
color:black;mso-fareast-language:FR'>Toute utilisation des Zones interactives
ou d'autres sections de ce Site en violation de ce qui précède viole les termes
de cet Accord et peut entraîner, notamment, l'annulation ou la suspension de
vos droits d'utilisation des Zones interactives et/ou de ce Site.<p></p></span></p>

                                                                                    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
text-align:justify;line-height:normal;mso-outline-level:4'><b><span
                                                                                                    style='font-size:13.5pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black;mso-fareast-language:FR'>RÉSERVATION AUPRÈS DE
FOURNISSEURS TIERS PAR LE BIAIS DE TRECO<p></p></span></b></p>

                                                                                    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
text-align:justify;line-height:normal'><b><span style='font-size:13.5pt;
font-family:"Arial",sans-serif;mso-fareast-font-family:"Times New Roman";
color:black;mso-fareast-language:FR'>Utiliser le système de réservation et
achat instantanés.&nbsp;</span></b><span style='font-size:13.5pt;font-family:
"Arial",sans-serif;mso-fareast-font-family:"Times New Roman";color:black;
mso-fareast-language:FR'>TRECO vous donne la possibilité de chercher,
sélectionner réserver, et acheter des formations professionnelles auprès de
fournisseurs tiers sans quitter le site internet de TRECO. La réservation de
votre formation, facilitée par le système de réservation et d’achat instantanés,
vous donnera accès au statut de membre, à moins que vous ne le soyez déjà. Les
membres de TRECO peuvent partager leurs expériences, participer à des forums de
discussion, envoyer des contenus TRECO aux autres membres, recevoir la
newsletter réservée aux membres et des informations destinées à faciliter la
préparation de leur formation, participer à des enquêtes, des concours ou des
loteries.<p></p></span></p>

                                                                                    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
text-align:justify;line-height:normal'><span style='font-size:13.5pt;
font-family:"Arial",sans-serif;mso-fareast-font-family:"Times New Roman";
color:black;mso-fareast-language:FR'>En utilisant le système de réservation et
d’achat instantanés, vous reconnaissez que vous acceptez les pratiques décrites
dans notre &nbsp;</span><u><span style='font-size:13.5pt;font-family:"Arial",sans-serif;
mso-fareast-font-family:"Times New Roman";color:#00AF87;mso-fareast-language:
FR'><a href="https://www.tripadvisor.fr/pages/privacy.html"><span
                                                                                                            style='color:#00AF87'>Charte sur le respect de la vie privée</span></a></span></u><span
                                                                                                style='font-size:13.5pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black;mso-fareast-language:FR'>&nbsp;ainsi que
l'entièreté de ces conditions générales d'utilisation. En outre, vous
garantissez que vous avez 18 ans ou plus, que vous disposez de la capacité
juridique pour conclure cet Accord et utiliser le système de réservation et d’achat
instantanés et ce Site, conformément aux présentes conditions générales
d'utilisation, et que toutes les informations que vous fournissez sont vraies
et précises. Vous acceptez également de n'utiliser le système de réservation et
achat instantanés que pour effectuer des réservations légitimes pour vous ou
des tiers au nom desquels vous êtes légalement autorisé à agir. Toute
réservation fausse ou frauduleuse est interdite et il pourrait être mis fin à
la participation de l'utilisateur qui tenterait d'effectuer ce genre de
réservation. Si vous possédez un compte TRECO, vous protégerez les
renseignements relatifs à votre compte et que vous superviserez et serez
entièrement responsable de toute utilisation de votre compte par un tiers.<p></p></span></p>

                                                                                    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
text-align:justify;line-height:normal'><span style='font-size:13.5pt;
font-family:"Arial",sans-serif;mso-fareast-font-family:"Times New Roman";
color:black;mso-fareast-language:FR'>EN TANT QU'UTILISATEUR DE CE SITE, Y
COMPRIS DU SYSTÈME DE RÉSERVATION ET ACHAT INSTANTANÉS, VOUS COMPRENEZ ET
CONVENEZ QUE&nbsp;: (1) NI TRECO NI SES AFFILIÉS NE POURRONT ÊTRE TENUS POUR
RESPONSABLES DE TRANSACTIONS NON AUTORISÉES EFFECTUÉES À L'AIDE DE VOTRE MOT DE
PASSE OU DE VOTRE COMPTE&nbsp;; ET (2) L'UTILISATION NON AUTORISÉE DE VOTRE MOT
DE PASSE OU DE VOTRE COMPTE PEUT ENGAGER VOTRE RESPONSABILITÉ ENVERS TRECO ET
D'AUTRES UTILISATEURS.<p></p></span></p>

                                                                                    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
text-align:justify;line-height:normal'><span style='font-size:13.5pt;
font-family:"Arial",sans-serif;mso-fareast-font-family:"Times New Roman";
color:black;mso-fareast-language:FR'>Lorsque vous effectuerez une réservation
par l'entremise du système de réservation et achat instantanés, nous
compilerons les informations relatives à votre paiement et nous les
transmettrons au fournisseur afin d'achever la transaction, comme décrit dans
notre&nbsp;Charte sur le respect de la vie privée. Veuillez noter que TRECO
traite le paiement mais que le fournisseur est responsable de la réservation.<p></p></span></p>

                                                                                    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
text-align:justify;line-height:normal'><span style='font-size:13.5pt;
font-family:"Arial",sans-serif;mso-fareast-font-family:"Times New Roman";
color:black;mso-fareast-language:FR'>TRECO et ses affiliés ne s'immisceront pas
dans les réservations de manière arbitraire, mais nous nous réservons le droit
d'annuler ou de ne pas traiter une réservation en raison de circonstances
exceptionnelles (par exemple, une réservation n'est plus disponible ou bien
nous avons des motifs raisonnables de penser qu'une demande de réservation est
frauduleuse). TRECO se réserve également le droit de vérifier votre identité
avant de traiter votre réservation.<p></p></span></p>

                                                                                    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
text-align:justify;line-height:normal'><span style='font-size:13.5pt;
font-family:"Arial",sans-serif;mso-fareast-font-family:"Times New Roman";
color:black;mso-fareast-language:FR'>Dans l'éventualité très improbable où une
réservation serait disponible lors de votre commande et indisponible avant
votre arrivée, votre seul recours sera de contacter le fournisseur afin de
prendre d'autres dispositions ou d'annuler votre réservation.<p></p></span></p>

                                                                                    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
text-align:justify;line-height:normal'><b><span style='font-size:13.5pt;
font-family:"Arial",sans-serif;mso-fareast-font-family:"Times New Roman";
color:black;mso-fareast-language:FR'>Fournisseurs tiers.&nbsp;</span></b><span
                                                                                                style='font-size:13.5pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black;mso-fareast-language:FR'>TRECO n'est pas une
agence de formations professionnelles et, par conséquent, ne fournit ni ne
possède de services de d’accueil et de formation. Bien que TRECO fournisse des
informations relatives aux produits de fournisseurs tiers et facilite les
réservations auprès de certains fournisseurs par l'entremise de son système de réservation
et achat instantanés, cela n'implique, ne suggère ni ne constitue en aucune
manière le parrainage ou l'approbation de fournisseurs tiers ni un partenariat
entre TRECO et des fournisseurs tiers. Bien que les membres de TRECO aient la
possibilité d'évaluer des établissements spécifiques en se basant sur leur
expérience personnelle, TRECO n'avalise ni ne recommande les produits ou les
services de fournisseurs tiers. Vous reconnaissez que TRECO n'est pas
responsable de l'exactitude ou de l'exhaustivité des informations fournies par
des fournisseurs tiers et qu'il affiche sur ses sites ou ses applications.<p></p></span></p>

                                                                                    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
text-align:justify;line-height:normal'><span style='font-size:13.5pt;
font-family:"Arial",sans-serif;mso-fareast-font-family:"Times New Roman";
color:black;mso-fareast-language:FR'>Si vous effectuez une réservation auprès
d'un fournisseur tiers, vous acceptez de lire et de respecter les conditions
générales d'achat et d'utilisation du site du fournisseur (les
«&nbsp;conditions générales&nbsp;»), la politique de confidentialité et toute
autre règle ou politique se rapportant au site ou aux biens du fournisseur.
Vous êtes responsable de vos interactions avec des fournisseurs tiers. TRECO
n'est en aucun cas responsable des actes, omissions, erreurs, déclarations,
garanties, violations ou négligences des fournisseurs tiers ni des dommages
corporels, décès, dommages aux biens, ou autres dommages ou frais résultant de
vos interactions avec des fournisseurs tiers.<p></p></span></p>

                                                                                    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
text-align:justify;line-height:normal'><span style='font-size:13.5pt;
font-family:"Arial",sans-serif;mso-fareast-font-family:"Times New Roman";
color:black;mso-fareast-language:FR'>Ce Site peut vous rediriger vers le site
de fournisseurs ou d'autres sites que TRECO ne gère ni ne contrôle. Pour de
plus amples informations, veuillez consulter la section «&nbsp;Liens vers des
sites tiers&nbsp;» ci-dessous.<p></p></span></p>

                                                                                    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
text-align:justify;line-height:normal;mso-outline-level:4'><b><span
                                                                                                    style='font-size:13.5pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black;mso-fareast-language:FR'>CLAUSE DE
NON-RESPONSABILITÉ<p></p></span></b></p>

                                                                                    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
text-align:justify;line-height:normal'><span style='font-size:13.5pt;
font-family:"Arial",sans-serif;mso-fareast-font-family:"Times New Roman";
color:black;mso-fareast-language:FR'>PRIÈRE DE LIRE CETTE SECTION AVEC
ATTENTION. CETTE SECTION LIMITE LA RESPONSABILITÉ DE TRECO CONCERNANT DES
PROBLÈMES QUI PEUVENT SURVENIR LORSQUE VOUS UTILISEZ CE SITE. SI VOUS NE
COMPRENEZ PAS LE CONTENU DE CETTE SECTION OU CELUI D'AUTRES SECTIONS DE
L'ACCORD, VEUILLEZ VOUS ADRESSER À UN AVOCAT AVANT D'UTILISER CE SITE OU D'Y
ACCÉDER.<p></p></span></p>

                                                                                    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
text-align:justify;line-height:normal'><span style='font-size:13.5pt;
font-family:"Arial",sans-serif;mso-fareast-font-family:"Times New Roman";
color:black;mso-fareast-language:FR'>LES INFORMATIONS, LOGICIELS, PRODUITS ET
SERVICES PUBLIÉS SUR CE SITE PEUVENT CONTENIR DES INEXACTITUDES OU DES ERREURS,
NOTAMMENT POUR CE QUI CONCERNE LE PROGRAMME <span
                                                                                                    style='mso-spacerun:yes'> </span>LES DISPONIBILITÉS ET LES TARIFS. TRECO, NE
GARANTIT PAS L'EXACTITUDE DES CONTENUS ET DÉCLINE TOUTE RESPONSABILITÉ QUANT
AUX ERREURS OU INEXACTITUDES CONCERNANT LES INFORMATIONS ET LA DESCRIPTION DE L’OFFRE
DE FORMATION SUR CE SITE (Y COMPRIS, SANS RESTRICTION, LES TARIFS, LES
DISPONIBILITÉS, LES PHOTOGRAPHIES, LA DESCRIPTION GÉNÉRALE DES FORMATIONS, LES
AVIS ET LES NOTES, ETC.). DE PLUS, TRECO SE RÉSERVE LE DROIT DE CORRIGER TOUTE
ERREUR DE PRIX OU DE DISPONIBILITÉ APPARAISSANT SUR SON SITE ET/OU DANS LES
RÉSERVATIONS EN COURS DONT LE PRIX ÉTAIT INCORRECT.<p></p></span></p>

                                                                                    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
text-align:justify;line-height:normal'><span style='font-size:13.5pt;
font-family:"Arial",sans-serif;mso-fareast-font-family:"Times New Roman";
color:black;mso-fareast-language:FR'>TRECO NE PREND EN AUCUN CAS POSITION PAR
RAPPORT À LA PERTINENCE DES INFORMATIONS, DES LOGICIELS, DES PRODUITS ET DES
SERVICES CONTENUS SUR CE SITE (CONTENU DU SITE), EN TOTALITÉ OU EN PARTIE, ET
LA PRÉSENCE, SUR CE SITE, D'OFFRES RELATIVES À DES PRODUITS OU DES SERVICES NE
SAURAIT CONSTITUER UNE APPROBATION OU UNE RECOMMANDATION DE CES PRODUITS OU
SERVICES PAR TRECO. CES INFORMATIONS, LOGICIELS, PRODUITS ET SERVICES SONT
FOURNIS «&nbsp;TELS QUELS&nbsp;» SANS GARANTIE D'AUCUNE SORTE. TRECO DÉCLINE
TOUTE RESPONSABILITÉ QUANT AUX GARANTIES, CONDITIONS OU AUTRES DISPOSITIONS QUI
AFFIRMENT QUE CE SITE, SES SERVEURS ET LES COURRIERS ÉLECTRONIQUES ENVOYÉS PAR
LES ENTREPRISES DE TRECO SONT EXEMPTS DE VIRUS OU D'AUTRES COMPOSANTS
NUISIBLES. DANS LES LIMITES PRÉVUES PAR LA LÉGISLATION EN VIGUEUR, TRECO
DÉCLINE TOUTE RESPONSABILITÉ QUANT AUX GARANTIES ET CONDITIONS CONCERNANT CES
INFORMATIONS, LOGICIELS, PRODUITS ET SERVICES, Y COMPRIS TOUTE GARANTIE OU
CONDITION TACITE CONCERNANT LA QUALITÉ MARCHANDE, L'ADÉQUATION À UN OBJET
PARTICULIER, LE DROIT DE PROPRIÉTÉ EN QUESTION, L'ÉVICTION ET L'ABSENCE
D'INFRACTION.<p></p></span></p>

                                                                                    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
text-align:justify;line-height:normal'><span style='font-size:13.5pt;
font-family:"Arial",sans-serif;mso-fareast-font-family:"Times New Roman";
color:black;mso-fareast-language:FR'>TRECO DÉCLINE EXPRESSÉMENT TOUTE
RESPONSABILITÉ QUANT AUX GARANTIES, DÉCLARATIONS OU AUTRES CONDITIONS
CONCERNANT L'EXACTITUDE OU LE CARACTÈRE EXCLUSIF DU CONTENU DU SITE.<p></p></span></p>

                                                                                    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
text-align:justify;line-height:normal'><span style='font-size:13.5pt;
font-family:"Arial",sans-serif;mso-fareast-font-family:"Times New Roman";
color:black;mso-fareast-language:FR'>RIEN DANS LE PRÉSENT ACCORD N'EXCLUT NI NE
LIMITE LA RESPONSABILITÉ DE TRECO EN MATIÈRE DE (I) DÉCÈS OU DE DOMMAGE
CORPOREL DÛS À UNE NÉGLIGENCE; (II) FRAUDE; (III) FAUSSE DÉCLARATION; (IV)
INFRACTION DÉLIBÉRÉE OU NÉGLIGENCE GRAVE; (V) TOUTE AUTRE RESPONSABILITÉ QUI NE
PEUT ÊTRE EXCLUE EN VERTU DE LA LÉGISLATION EN VIGUEUR.<p></p></span></p>

                                                                                    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
text-align:justify;line-height:normal'><span style='font-size:13.5pt;
font-family:"Arial",sans-serif;mso-fareast-font-family:"Times New Roman";
color:black;mso-fareast-language:FR'>LES FOURNISSEURS TIERS QUI, SUR CE SITE,
COMMUNIQUENT DES INFORMATIONS SUR LES FORMATIONS. TRECO N’EST PAS RESPONSABLE
DES ACTES, ERREURS, OMISSIONS, DÉCLARATIONS, GARANTIES, VIOLATIONS OU
NÉGLIGENCES DE LA PART DES FOURNISSEURS NI DES DOMMAGES CORPORELS, DÉCÈS,
DOMMAGES MATÉRIELS OU AUTRES DOMMAGES OU DÉPENSES EN RÉSULTANT. DECLINE TOUTE
RESPONSABILITÉ ET NE PROCÉDERA À AUCUN REMBOURSEMENT EN CAS DE RETARD,
ANNULATION, SURRÉSERVATION, GRÈVE, CAS DE FORCE MAJEURE OU AUTRES CAUSES
ÉCHAPPANT À LEUR CONTRÔLE ET DÉCLINE TOUTE RESPONSABILITÉ EN CAS DE FRAIS
SUPPLÉMENTAIRES, OMISSIONS, RETARDS, RÉACHEMINEMENT OU DÉCISIONS DE
GOUVERNEMENTS OU D'AUTORITÉS.<p></p></span></p>

                                                                                    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
text-align:justify;line-height:normal'><span style='font-size:13.5pt;
font-family:"Arial",sans-serif;mso-fareast-font-family:"Times New Roman";
color:black;mso-fareast-language:FR'>SOUS RÉSERVE DE CE QUI PRÉCÈDE, VOUS
UTILISEZ CE SITE À VOS PROPRES RISQUES ET TRECO NE SAURAIT ÊTRE, EN AUCUN CAS,
TENUE POUR RESPONSABLE DES DOMMAGES OU PERTES DIRECTS, INDIRECTS, PUNITIFS,
EXTRAORDINAIRES OU CONSÉCUTIFS OU DES PERTES DE REVENU, DE PROFIT, DE
CLIENTÈLE, DE DONNÉES, DE CONTRATS, D'UTILISATION D'ARGENT OU DE DOMMAGES ET
PERTES DÉCOULANT D'UNE INTERRUPTION D'ACTIVITÉ, DE VOTRE ACCÈS, DE L'AFFICHAGE
OU DE L'UTILISATION DE CE SITE, DE L'IMPOSSIBILITÉ D'Y ACCÉDER, DE L'AFFICHER
OU DE L'UTILISER (Y COMPRIS, MAIS SANS S'Y LIMITER, LA CONFIANCE ACCORDÉE AUX
AVIS ET OPINIONS APPARAISSANT SUR LE SITE&nbsp;; TOUT VIRUS INFORMATIQUE,
INFORMATION, LOGICIEL, SITES LIÉS, PRODUITS ET SERVICES OBTENUS GRÂCE À CE
SITE&nbsp;; OU DÉCOULANT DE L'ACCÈS, DE L'AFFICHAGE OU DE L'UTILISATION DE CE
SITE) QU'ILS SOIENT BASÉS SUR UNE NÉGLIGENCE, CONTRAT, TORT, RESPONSABILITÉ
STRICTE OU AUTRE ET MÊME SI TRECO A ÉTÉ INFORMÉ DE LA POSSIBILITÉ DE TELS
DOMMAGES.<p></p></span></p>

                                                                                    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
text-align:justify;line-height:normal'><span style='font-size:13.5pt;
font-family:"Arial",sans-serif;mso-fareast-font-family:"Times New Roman";
color:black;mso-fareast-language:FR'>CES CONDITIONS D'UTILISATION ET LA CLAUSE
DE NON-RESPONSABILITÉ N'INFLUENCENT PAS LES DROITS JURIDIQUES OBLIGATOIRES QUI
NE PEUVENT PAS ÊTRE EXCLUS CONFORMÉMENT À LA LÉGISLATION EN VIGUEUR.<p></p></span></p>

                                                                                    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
text-align:justify;line-height:normal'><span style='font-size:13.5pt;
font-family:"Arial",sans-serif;mso-fareast-font-family:"Times New Roman";
color:black;mso-fareast-language:FR'>Si TRECO est tenue pour responsable de
dommage ou de perte qui résultent de l'utilisation de notre site ou de nos
services ou qui y sont liés, en aucun cas la responsabilité de TRECO ne
dépassera, au total, la plus grande partie (a) des frais de transaction payés à
TRECO pour la(les) transaction(s) effectuée(s) sur ce site à l'origine de la
réclamation, ou (b) cent euro (US 100,00€).<p></p></span></p>

                                                                                    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
text-align:justify;line-height:normal'><span style='font-size:13.5pt;
font-family:"Arial",sans-serif;mso-fareast-font-family:"Times New Roman";
color:black;mso-fareast-language:FR'>La limitation de responsabilité reflète la
répartition des risques entre les parties. Les limitations prévues à la
présente section s'appliqueront même s’il s’avère qu’un recours limité, quel
qu’il soit, spécifié dans ces conditions n’a pas rempli sa fonction
essentielle. Les limitations de responsabilité prévues dans ces conditions
d'utilisation s'appliqueront au profit de TRECO<p></p></span></p>

                                                                                    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
text-align:justify;line-height:normal;mso-outline-level:4'><b><span
                                                                                                    style='font-size:13.5pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black;mso-fareast-language:FR'>INDEMNISATION<p></p></span></b></p>

                                                                                    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
text-align:justify;line-height:normal'><span style='font-size:13.5pt;
font-family:"Arial",sans-serif;mso-fareast-font-family:"Times New Roman";
color:black;mso-fareast-language:FR'>Vous acceptez de défendre et d'indemniser TRECO,
ses affiliés, ses responsables, ses dirigeants, ses employés et ses agents face
à toute réclamation, cause d'action, demande, remboursement, perte, dommage,
frais, pénalité ou autres coûts ou dépenses incluant, mais sans s'y limiter,
les frais d'avocats et de comptabilité pour un montant raisonnable, intentés
par des tiers, résultant de :<p></p></span></p>

                                                                                    <ul type=disc>
                                                                                        <li class=MsoNormal style='color:black;mso-margin-top-alt:auto;mso-margin-bottom-alt:
     auto;text-align:justify;line-height:normal;mso-list:l1 level1 lfo3;
     tab-stops:list 36.0pt'><span style='font-size:13.5pt;font-family:"Arial",sans-serif;
     mso-fareast-font-family:"Times New Roman";mso-fareast-language:FR'>(i)
     votre violation de cet Accord ou des documents mentionnés ici&nbsp;;<p></p></span></li>
                                                                                        <li class=MsoNormal style='color:black;mso-margin-top-alt:auto;mso-margin-bottom-alt:
     auto;text-align:justify;line-height:normal;mso-list:l1 level1 lfo3;
     tab-stops:list 36.0pt'><span style='font-size:13.5pt;font-family:"Arial",sans-serif;
     mso-fareast-font-family:"Times New Roman";mso-fareast-language:FR'>(ii)
     votre violation d'une loi ou des droits d’une tierce partie&nbsp;; ou<p></p></span></li>
                                                                                        <li class=MsoNormal style='color:black;mso-margin-top-alt:auto;mso-margin-bottom-alt:
     auto;text-align:justify;line-height:normal;mso-list:l1 level1 lfo3;
     tab-stops:list 36.0pt'><span style='font-size:13.5pt;font-family:"Arial",sans-serif;
     mso-fareast-font-family:"Times New Roman";mso-fareast-language:FR'>(iii)
     votre utilisation de ce Site.<p></p></span></li>
                                                                                    </ul>

                                                                                    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
text-align:justify;line-height:normal;mso-outline-level:4'><b><span
                                                                                                    style='font-size:13.5pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black;mso-fareast-language:FR'>LOGICIELS DISPONIBLES
SUR CE SITE<p></p></span></b></p>

                                                                                    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
text-align:justify;line-height:normal'><span style='font-size:13.5pt;
font-family:"Arial",sans-serif;mso-fareast-font-family:"Times New Roman";
color:black;mso-fareast-language:FR'>Veuillez noter que tous les Logiciels, y
compris, sans restriction, tous les codes HTML, XML, Java et les contrôles
Active X contenus sur ce site sont la propriété de TRECO sur le droit d'auteur
et des dispositions des traités internationaux. Toute reproduction ou
redistribution des Logiciels est strictement interdite et peut entraîner de
graves sanctions civiles et pénales. Les contrevenants seront poursuivis dans
toute la mesure du possible.<p></p></span></p>

                                                                                    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
text-align:justify;line-height:normal'><span style='font-size:13.5pt;
font-family:"Arial",sans-serif;mso-fareast-font-family:"Times New Roman";
color:black;mso-fareast-language:FR'>SANS LIMITER CE QUI PRÉCÈDE, LA COPIE OU
LA REPRODUCTION DES LOGICIELS, SUR TOUT AUTRE SERVEUR OU EMPLACEMENT, POUR
TOUTE REPRODUCTION OU REDISTRIBUTION ULTÉRIEURE, EST STRICTEMENT INTERDITE. LE
LOGICIEL N'EST ÉVENTUELLEMENT GARANTI QUE CONFORMÉMENT AUX TERMES DE L'ACCORD
DE LICENCE.<p></p></span></p>

                                                                                    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
text-align:justify;line-height:normal;mso-outline-level:4'><b><span
                                                                                                    style='font-size:13.5pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black;mso-fareast-language:FR'>DROITS D'AUTEUR ET
MARQUES<p></p></span></b></p>

                                                                                    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
text-align:justify;line-height:normal'><span style='font-size:13.5pt;
font-family:"Arial",sans-serif;mso-fareast-font-family:"Times New Roman";
color:black;mso-fareast-language:FR'>Tous les contenus de ce Site sont&nbsp;:
©2018 TRECO. Tous droits réservés. TRECO n'est pas responsable du contenu de
sites exploités par des tiers. TRECO, le logo, les notes et tout autre nom de
produit ou de service apparaissant sur ce Site sont des marques déposées et/ou
de droit commun de TRECO et/ou de ses fournisseurs ou donneurs de licences. Ils
ne peuvent pas être copiés, imités ni utilisés, en totalité ou en partie, sans
l'autorisation écrite et préalable de TRECO ou du propriétaire de la marque
concerné. De plus, la présentation et le mode de fonctionnement de ce Site, y
compris tous les en-têtes, images personnalisées, icônes et textes, sont des
marques de service, marques et/ou présentations de TRECO et ne peuvent être
copiés, imités ou utilisés, en totalité ou en partie, sans l'autorisation
écrite et préalable de TRECO. Toutes les autres marques, marques déposées, noms
de produits et noms d'entreprise ou logos mentionnés sur ce Site appartiennent
à leurs propriétaires respectifs. Toute référence à des produits, services,
processus ou autres informations, par un nom commercial, une marque, un
fabricant, un fournisseur ou autre, ne saurait constituer une approbation, un
parrainage ou une recommandation de la part de TRECO.<p></p></span></p>

                                                                                    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
text-align:justify;line-height:normal'><b><span style='font-size:13.5pt;
font-family:"Arial",sans-serif;mso-fareast-font-family:"Times New Roman";
color:black;mso-fareast-language:FR'>Politique de notification et retrait pour
contenu illicite</span></b><span style='font-size:13.5pt;font-family:"Arial",sans-serif;
mso-fareast-font-family:"Times New Roman";color:black;mso-fareast-language:
FR'><p></p></span></p>

                                                                                    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
text-align:justify;line-height:normal'><span style='font-size:13.5pt;
font-family:"Arial",sans-serif;mso-fareast-font-family:"Times New Roman";
color:black;mso-fareast-language:FR'>TRECO opère selon le principe
«&nbsp;notification et retrait&nbsp;». En cas de réclamation ou d'objection de
votre part quant à des éléments ou des contenus placés sur le Site, ou si vous
pensez que des éléments ou des contenus placés sur le Site violent vos droits
d'auteur, veuillez nous contacter immédiatement, en suivant notre procédure de
notification et retrait.&nbsp;Cliquez ici pour visualiser notre procédure de
notification et retrait. Au terme de cette procédure, TRECO s'efforcera, dans
la mesure du possible, de supprimer le contenu illicite dans les meilleurs
délais.<p></p></span></p>

                                                                                    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
text-align:justify;line-height:normal'><b><span style='font-size:13.5pt;
font-family:"Arial",sans-serif;mso-fareast-font-family:"Times New Roman";
color:black;mso-fareast-language:FR'>Modifications</span></b><span
                                                                                                style='font-size:13.5pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black;mso-fareast-language:FR'><p></p></span></p>

                                                                                    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
text-align:justify;line-height:normal'><span style='font-size:13.5pt;
font-family:"Arial",sans-serif;mso-fareast-font-family:"Times New Roman";
color:black;mso-fareast-language:FR'>TRECO peut, à son entière discrétion,
modifier, enrichir ou supprimer ces conditions générales, en totalité ou en
partie, à des fins juridiques, techniques ou réglementaires, s'il le juge
nécessaire ou à la suite d'une modification des services fournis, de la nature
ou de la présentation du Site. Vous consentez expressément à être lié par les
conditions générales qui ont été modifiées.<p></p></span></p>

                                                                                    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
text-align:justify;line-height:normal'><span style='font-size:13.5pt;
font-family:"Arial",sans-serif;mso-fareast-font-family:"Times New Roman";
color:black;mso-fareast-language:FR'>TRECO se réserve le droit de modifier,
suspendre ou interrompre tout aspect des services TRECO, notamment la
disponibilité des options, bases de données ou contenus. TRECO peut également
limiter certaines options et certains services ou restreindre votre accès à
tout ou partie du Site ou de tout autre site internet de TRECO, sans préavis ni
aucune autre obligation, pour des raisons techniques ou de sécurité, pour
empêcher un accès non autorisé, la perte ou la destruction de données, ou
lorsque nous considérons, à notre entière discrétion, que vous enfreignez les
dispositions des présentes conditions générales, d'une loi ou d'un règlement et
lorsque nous décidons d'arrêter la fourniture d'un service.<p></p></span></p>

                                                                                    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
text-align:justify;line-height:normal'><span style='font-size:13.5pt;
font-family:"Arial",sans-serif;mso-fareast-font-family:"Times New Roman";
color:black;mso-fareast-language:FR'>EN CONTINUANT D'UTILISER LE SITE TRECO
MAINTENANT OU APRÈS QUE DES MODIFICATIONS ONT ÉTÉ PUBLIÉES, VOUS SIGNIFIEZ QUE
VOUS ACCEPTEZ CES CHANGEMENTS.<p></p></span></p>

                                                                                    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
text-align:justify;line-height:normal;mso-outline-level:4'><b><span
                                                                                                    style='font-size:13.5pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black;mso-fareast-language:FR'>RÈGLEMENT DES LITIGES<p></p></span></b></p>

                                                                                    <p style='text-align:justify'><span style='font-size:13.5pt;font-family:"Arial",sans-serif;
color:black'>Les présentes Conditions Générales de Vente sont soumises au droit
français. En cas de contestations relatives à la formation, l'exécution,
l'interprétation et/ou la cessation du présent contrat, le litige relèvera de
la compétence exclusive du Tribunal de Commerce de Rouen, que ce soit en
référé, qu'il y ait appel en garantie ou pluralité de défendeurs.<p></p></span></p>

                                                                                    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
text-align:justify;line-height:normal'><span style='font-size:13.5pt;
font-family:"Arial",sans-serif;mso-fareast-font-family:"Times New Roman";
color:black;mso-fareast-language:FR'>©2018 Tous droits réservés.<p></p></span></p>

                                                                                    <p class=MsoNormal><p>&nbsp;</p></p>

                                                                                </div>
                                                                            </div>
                                                                            <div class="modal-footer">
                                                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </label>
                                                            <div class="invalid-feedback">
                                                                You must agree before submitting.
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                        <button type="button Submit" class="btn btn-primary">Send message</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
            <script src="../style/bootstrap.min.js"></script>
            <script src="../style/bootstrap.js"></script>
            <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8/jquery.min.js"></script>

        </section>

            
        <footer>
            <div class="container">
                <div class="row m-auto">
                    <div class="col-lg-7 m-auto archi">
                        <div class="row justify-content-between grand-titre">
                            <div class="col"><h3>Redirection</h3></div>
                            <div class="col"><h3>Préférences</h3></div>
                            <div class="col"><h3>Support</h3></div>
                        </div>
                        <div class="row justify-content-between">
                            <div class="col"><a href="../index.php"><p class="texte">Accueil</p></a></div>
                            <div class="col"><p class="texte">Web</p></div>
                            <div class="col"><p class="texte"><a href="Contacts.php" style="color:white">Contact</a></p></div>
                        </div>
                        <div class="row justify-content-between">
                            <div class="col"><a href="Recherche.php"><p class="texte">Recherche</p></a></div>
                            <div class="col"><p class="texte">Marketing</p></div>
                            <div class="col"><p class="texte"></p></div>
                        </div>
                        <div class="row justify-content-between">
                            <div class="col"><a href="#"><p class="texte">Organisme</p></a></div>
                            <div class="col"><p class="texte"></p></div>
                            <div class="col"><p class="texte"></p></div>
                        </div>
                    </div>
                    <div class="col-lg-3 PR m-auto">
                        <table>
                            <tr><td><a href="https://www.facebook.com/treco.tech/" target="_blank"><img src="../images/fb.png" width="24px"/></a></td></tr>
                            <tr><td><a href="https://fr.linkedin.com/company/treco-tech" target="_blank"><img src="../images/lkd.png" width="24px"/></a></td></tr>
                            <tr><td><a href="https://twitter.com/treco_tech" target="_blank"><img src="../images/tw.jpg" width="24px"/></a></td></tr>
                            <tr><td><a href="https://treco.tech" target="_blank"><img src="../images/Bl.png" width="24px"/></a></td></tr>
                        </table>
                        <img id="img_footer" src="../images/TRECO-white.png" width="200px" height="65px"/>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="copyright">Copyright © 2018. Tous droits réservés.</div>
                </div>
            </div>
        </footer> 
            
        <script type="text/javascript" language="JavaScript">
            <?php if(isset($_SESSION['message']) && $_SESSION['message'] != null){?> window.alert("<?php echo $_SESSION['message'];?>"); <?php } ?>
        </script>

        <?php $_SESSION['message'] = null; ?>
            
    </body>
</html>			