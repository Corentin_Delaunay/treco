<?php
session_start();
$_SESSION['page_actuelle']="Recherche.php";
if(!isset($_SESSION['etat'])){$_SESSION['etat']="deconnecte";}
if(!isset($_SESSION['type'])){$_SESSION['type']=null;}
if(!isset($_SESSION['id'])){$_SESSION['id']=null;}
include("Connexion.php");
//Tableau associatifs des différents filtres
$tab_modalite = array('presentiel','e-learning');
$tab_prix = array('0-100','100-200','200-500','500+');
$tab_nom_prix = array('Entre 0-100€','Entre 100-200€','Entre 200-500€','500 et +');
$f_nom_modalite = array();
$f_nom_prix = array();
$f_nom_domaine = '';
if(isset($_POST['modalite'])){
    $f_nom_modalite = array();
    foreach ($_POST['modalite'] as $modal) {
        $f_nom_modalite[] = $modal;
    }
}else{
    //$f_nom_modalite = array('presentiel','e-learning');
    $f_nom_modalite = array();
}
if(isset($_POST['prix'])){
    $f_nom_prix = array();
    foreach ($_POST['prix'] as $modal) {
        $f_nom_prix[] = $modal;
    }
}else{
    $f_nom_prix = array();
}
if(isset($_POST['domaine_2'])){
    $f_nom_domaine = $_POST['domaine_2'];
    if($_POST['domaine_2']==' '){
        $f_nom_domaine = '';
    }
    var_dump($f_nom_domaine);
}else{
    $f_nom_domaine = '';
}
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width" />
    <link href="../style/bootstrap.css" rel="stylesheet"/>
    <link href="../style/recherche.css" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    <link rel="shortcut icon" type="image/x-icon" href="../images/favicon.ico" />
    <title>TRECO sélectionne les meilleures formations pour vous</title>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-119675621-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-119675621-1');
    </script>
    <script type="text/javascript"> _linkedin_data_partner_id = "374204"; </script><script type="text/javascript"> (function(){var s = document.getElementsByTagName("script")[0]; var b = document.createElement("script"); b.type = "text/javascript";b.async = true; b.src = "https://snap.licdn.com/li.lms-analytics/insight.min.js"; s.parentNode.insertBefore(b, s);})(); </script> <noscript> <img height="1" width="1" style="display:none;" alt="" src="https://dc.ads.linkedin.com/collect/?pid=374204&fmt=gif
https://dc.ads.linkedin.com/collect/?pid=374204&fmt=gif
" /> </noscript>
</head>

<header class="site-header">
    <div class="container">
        <div class="row" >
            <div class="col-lg-3">
                <a href="../index.php"><img id="img_tete" src="../images/TRECO-white.png" width="60%"/></a>
            </div>
            <div class="row col-lg-6 mx-auto rechercher">
                <form method="post" action="Recherche.php" class="recherche">
                    <div class="row justify-content-between" width="100%">
                        <div class="col-5">
                            <input id="fonctionR" type="text" name="name" placeholder="Entrez votre recherche" />
                        </div>
                        <div class="col-1">
                            <button type="submit" class="btn btn-primary button-R"><i class="fas fa-search"></i></button>
                        </div>
                        <?php
                        $rqt_modalite = $bdd->query('SELECT * FROM at_modalite');
                        echo '<ul hidden>';
                        while($rep_modalite = $rqt_modalite->fetch()){
                            $rqt_nb_formation = $bdd->query('SELECT COUNT(*) as cpt FROM t_formation WHERE ID_MOD='.$rep_modalite['ID_MOD']);
                            $nb_formation = $rqt_nb_formation->fetch();
                            $nb_formation = $nb_formation['cpt'];
                            $insertion= false;
                            if($nb_formation!=0){
                                if(isset($_POST['modalite'])){
                                    foreach ($_POST['modalite'] as $modal){
                                        if($modal==$rep_modalite['REC_MOD']){
                                            echo '<li class="chk"><input type="checkbox" name="modalite[]" value="'.$rep_modalite['REC_MOD'].'" checked>'.$rep_modalite['NOM_MOD'].'</li>';
                                            $insertion = true;
                                        }
                                    }
                                }
                                if(!$insertion){
                                    echo '<li class="chk"><input type="checkbox" name="modalite[]" value="'.$rep_modalite['REC_MOD'].'">'.$rep_modalite['NOM_MOD'].'</li>';
                                }
                            }
                        }
                        echo '</ul>';

                        echo '<ul hidden>';
                        $count = count($tab_prix);
                        for($i=0;$i<$count;$i++){
                            $insertion = false;
                            if(isset($_POST['prix'])){
                                foreach ($_POST['prix'] as $modal){
                                    if($modal==$tab_prix[$i]){
                                        echo '<li class="chk"><input type="checkbox" name="prix[]" value="'.$tab_prix[$i].'" checked>'.$tab_nom_prix[$i].'</li>';
                                        $insertion = true;
                                    }
                                }
                            }
                            if(!$insertion){
                                echo '<li class="chk"><input type="checkbox" name="prix[]" value="'.$tab_prix[$i].'">'.$tab_nom_prix[$i].'</li>';
                            }
                        }
                        echo '</ul>';
                        ?>
                    </div>
                </form>
            </div>
            <div class="row col-lg-3 mx-auto navigation">
                <div class="col-lg-1 my-auto ">
                    <a id="menu" href="../index.php">Accueil</a>
                </div>
                <div class="col-lg-1 my-auto mx-auto">
                    <a id="menu" href="organisme-de-formation.php">Organisme</a>
                </div>
                <div class="col-lg-1 my-auto">
                    <a id="menu" href="Contacts.php">Contacts</a>
                </div>
            </div>
        </div>
    </div>
</header>

<body>

<div class="content">
    <div class="container ct">
        <div class="row justify-content-around">
            <div class="col-md-2" id="filtre">
                <h5>FILTRES</h5>
                <div class="filtre">
                    <form method="post">
                        <div class="mod">
                            <h6>Modalité pédagogique</h6>
                            <ul>
                                <?php
                                $rqt_modalite = $bdd->query('SELECT * FROM at_modalite');
                                while($rep_modalite = $rqt_modalite->fetch()){
                                    $rqt_nb_formation = $bdd->query('SELECT COUNT(*) as cpt FROM t_formation WHERE ID_MOD='.$rep_modalite['ID_MOD']);
                                    $nb_formation = $rqt_nb_formation->fetch();
                                    $nb_formation = $nb_formation['cpt'];
                                    $insertion= false;
                                    if($nb_formation!=0){
                                        if(isset($_POST['modalite'])){
                                            foreach ($_POST['modalite'] as $modal){
                                                if($modal==$rep_modalite['REC_MOD']){
                                                    echo '<li class="chk"><input type="checkbox" name="modalite[]" value="'.$rep_modalite['REC_MOD'].'" checked>'.$rep_modalite['NOM_MOD'].'</li>';
                                                    $insertion = true;
                                                }
                                            }
                                        }
                                        if(!$insertion){
                                            echo '<li class="chk"><input type="checkbox" name="modalite[]" value="'.$rep_modalite['REC_MOD'].'">'.$rep_modalite['NOM_MOD'].'</li>';
                                        }
                                    }
                                }
                                ?>
                            </ul>
                        </div>
                        <div class="price">
                            <h6>Prix</h6>
                            <ul>
                                <?php
                                $count = count($tab_prix);
                                for($i=0;$i<$count;$i++){
                                    $insertion = false;
                                    if(isset($_POST['prix'])){
                                        foreach ($_POST['prix'] as $modal){
                                            if($modal==$tab_prix[$i]){
                                                echo '<li class="chk"><input type="checkbox" name="prix[]" value="'.$tab_prix[$i].'" checked>'.$tab_nom_prix[$i].'</li>';
                                                $insertion = true;
                                            }
                                        }
                                    }
                                    if(!$insertion){
                                        echo '<li class="chk"><input type="checkbox" name="prix[]" value="'.$tab_prix[$i].'">'.$tab_nom_prix[$i].'</li>';
                                    }
                                }
                                ?>
                            </ul>
                        </div>
                        <div class="time">
                            <h6>Domaine de compétences</h6>
                            <ul>
                                <li class="chk">
                                    <SELECT name="domaine_2"  class="domaine_2" size="1">
                                        <?php
                                        $recherche_domaine2 = $bdd->query('SELECT * FROM at_domaine2');

                                        if($f_nom_domaine==''){
                                            echo '<option class="d2" ';
                                            echo 'selected="selected" ';
                                            echo 'value="">Choisissez un domaine</option>';
                                        }else{
                                            echo '<option class="d2" ';
                                            echo 'value="">Choisissez un domaine</option>';
                                        }
                                        while($donnees = $recherche_domaine2->fetch()){
                                            echo '<option class="d2" ';
                                            $insertion = false;
                                            if(isset($_POST['domaine_2'])){
                                                if($f_nom_domaine!='' && $f_nom_domaine==$donnees['NOM_D2']){
                                                    echo 'selected="selected" ';
                                                    echo 'value="'.$donnees['NOM_D2'].'">'.$donnees['NOM_D2'].'</option>';
                                                    $insertion = true;
                                                }
                                            }
                                            if(!$insertion){
                                                echo 'value="'.$donnees['NOM_D2'].'">'.$donnees['NOM_D2'].'</option>';
                                            }
                                        }
                                        $recherche_domaine2->closeCursor();
                                        ?>
                                    </SELECT>
                                </li>
                            </ul>
                        </div>
                        <br/>
                        <?php
                        if(isset($_POST['name']))
                            echo '<input id="fonctionR" type="text" name="name" value="'.$_POST['name'].'" hidden/>';
                        else
                            echo '<input id="fonctionR" type="text" name="name" hidden/>'
                        ?>
                        <button type="submit" class="btn-primary btn">Filtrer</button>
                    </form>
                </div>
            </div>
        </div>

        <div class="col-md-9 colonne">
            <div id="recherche">
                <?php
                //$requeteSql_formation = $bdd->query('SELECT * FROM t_formation WHERE PRIX <100.00');
                $count_moda = count($f_nom_modalite);
                $count_prix = count($f_nom_prix);
                if($count_moda!=0) {
                    for ($i = 0; $i < $count_moda; $i++) {
                        $conditions = '';
                        if($count_prix != 0){
                            for ($y = 0; $y < $count_prix; $y++) {
                                if($y==0){
                                    switch($f_nom_prix[$y]){
                                        case $tab_prix[0] : $conditions = 'AND ( PRIX < 100.00 )';break;
                                        case $tab_prix[1] : $conditions = 'AND ( PRIX > 100.00 AND PRIX < 200.00 )';break;
                                        case $tab_prix[2] : $conditions = 'AND ( PRIX > 200.00 AND PRIX < 500.00 )';break;
                                        case $tab_prix[3] : $conditions = 'AND ( PRIX > 500.00 )';break;
                                    }
                                }else {
                                    switch($f_nom_prix[$y]){
                                        case $tab_prix[0] : $conditions .= ' OR ( PRIX < 100.00 )';break;
                                        case $tab_prix[1] : $conditions .= ' OR ( PRIX > 100.00 AND PRIX < 200.00 )';break;
                                        case $tab_prix[2] : $conditions .= ' OR ( PRIX > 200.00 AND PRIX < 500.00 )';break;
                                        case $tab_prix[3] : $conditions .= ' OR ( PRIX > 500.00 )';break;
                                    }
                                }
                            }
                        }
                        $domaine='';
                        if($f_nom_domaine!='') {
                            $requeteID_domaine2 = $bdd->query('SELECT * FROM at_domaine2 WHERE NOM_D2 LIKE "%' . $f_nom_domaine . '%"');
                            $donnees_domaine2 = $requeteID_domaine2->fetch();
                            $domaine = 'AND ID_D2='.$donnees_domaine2['ID_D2'];
                        }
                        if (!isset($_POST['name'])) {
                            echo '<div class="container">';
                            $requeteSql_modalites = $bdd->query('SELECT * FROM at_modalite WHERE REC_MOD="' . $f_nom_modalite[$i] . '"');
                            $rec_modalite = $requeteSql_modalites->fetch();
                            $requete_formation = 'SELECT * FROM t_formation WHERE ID_MOD=' . $rec_modalite['ID_MOD'] .' '.$conditions.' '.$domaine;
                            $requeteSql_formation = $bdd->query($requete_formation);
                            while ($donnees_formation = $requeteSql_formation->fetch()) {
                                echo '<div class="row">';
                                echo '<div class="container article">';
                                echo '<div class="row align-items-center">';
                                echo '<div class="col-2">';
                                echo '<img src="../images/Informatique.jpg" width="100%" class="photo"></div>';
                                echo '<div class="col">';
                                echo '<h3 class="title">' . $donnees_formation['TITRE'] . '</h3></div>';
                                echo '<div class="col btn-2">';
                                echo '<a href="fiche-formation.php?numero=' . $donnees_formation['ID_FORM'] . '&nom=' . $donnees_formation['TITRE'] . '" target="_blank" class="button-2"><i class="fas fa-plus"></i><span>&nbsp;Plus d\'info</span></a></div></div>';
                                echo '<div class="row">';
                                echo '<div class="col-lg-12 desc">';
                                $requeteSql_description = $bdd->prepare('SELECT * FROM t_description WHERE ID_DESCRIPTION = ?');
                                $requeteSql_description->execute(array($donnees_formation['ID_DESCRIPTION']));
                                $donnees_description = $requeteSql_description->fetch();
                                echo '<h4 class="overflow-ellipsis; overflow-visible;">' . $donnees_description['CHAPEAU'] . '</h4>';
                                $requeteSql_description->closeCursor();
                                echo '</div></div>';
                                echo '<div class="row align-items-center overlay">';
                                echo '<div class="col-3 ">';
                                echo '<h5 class="li">' . $donnees_formation['DUREE'] . '</h5></div>';
                                $requeteSql_modalite = $bdd->query('SELECT * FROM at_modalite WHERE ID_MOD =' . $donnees_formation['ID_MOD']);
                                $donnees_modalite = $requeteSql_modalite->fetch();
                                echo '<div class="col-3 mx-auto"><h5 class="li">' . $donnees_modalite['NOM_MOD'] . '</h5></div>';
                                $requeteSql_modalite->closeCursor();
                                echo '<div class="col-3 mx-auto"><h5 class="li">' . $donnees_formation['PRIX'] . '€</h5></div>';
                                $requeteSql_organisme = $bdd->query('SELECT * FROM t_organisation WHERE ID_ORGA = ' . $donnees_formation['ID_ORGA']);
                                $donnees_organisme = $requeteSql_organisme->fetch();
                                echo '</div>';
                                echo '</div>';
                                echo '</div>';
                                $requeteSql_description->closeCursor();
                                $requeteSql_modalite->closeCursor();
                                $requeteSql_organisme->closeCursor();
                            }
                            $requeteSql_formation->closeCursor();
                            $requeteSql_modalites->closeCursor();
                        } else {
                            echo '<div class="container">';
                            $requeteSQL_modalite = $bdd->query('SELECT * FROM at_modalite WHERE REC_MOD="' . $f_nom_modalite[$i] . '"' );
                            $rec_modalite = $requeteSQL_modalite->fetch();
                            $requete_formation = 'SELECT * FROM t_formation WHERE ID_MOD=' . $rec_modalite['ID_MOD'] .' AND TITRE LIKE "%' . $_POST['name'] . '%" '.$conditions.' '.$domaine;
                            $requeteSql_formation = $bdd->query($requete_formation);
                            while ($donnees_formation = $requeteSql_formation->fetch()) {
                                echo '<div class="row">';
                                echo '<div class="container article">';
                                echo '<div class="row align-items-center">';
                                echo '<div class="col-2">';
                                echo '<img src="../images/Informatique.jpg" width="100%" class="photo"></div>';
                                echo '<div class="col">';
                                echo '<h3 class="title">' . $donnees_formation['TITRE'] . '</h3></div>';
                                echo '<div class="col btn-2">';
                                echo '<a href="fiche-formation.php?numero=' . $donnees_formation['ID_FORM'] . '&nom=' . $donnees_formation['TITRE'] . '" target="_blank" class="button-2"><i class="fas fa-plus"></i><span>&nbsp;Plus d\'info</span></a></div></div>';
                                echo '<div class="row">';
                                echo '<div class="col-lg-12 desc">';
                                $requeteSql_description = $bdd->prepare('SELECT * FROM t_description WHERE ID_DESCRIPTION = ?');
                                $requeteSql_description->execute(array($donnees_formation['ID_DESCRIPTION']));
                                $donnees_description = $requeteSql_description->fetch();
                                echo '<h4 class="overflow-ellipsis; overflow-visible;">' . $donnees_description['CHAPEAU'] . '</h4>';
                                $requeteSql_description->closeCursor();
                                echo '</div></div>';
                                echo '<div class="row align-items-center overlay">';
                                echo '<div class="col-3 ">';
                                echo '<h5 class="li">' . $donnees_formation['DUREE'] . '</h5></div>';
                                $requeteSql_modalite = $bdd->query('SELECT * FROM at_modalite WHERE ID_MOD =' . $donnees_formation['ID_MOD']);
                                $donnees_modalite = $requeteSql_modalite->fetch();
                                echo '<div class="col-3 mx-auto"><h5 class="li">' . $donnees_modalite['NOM_MOD'] . '</h5></div>';
                                $requeteSql_modalite->closeCursor();
                                echo '<div class="col-3 mx-auto"><h5 class="li">' . $donnees_formation['PRIX'] . '€</h5></div>';
                                $requeteSql_organisme = $bdd->query('SELECT * FROM t_organisation WHERE ID_ORGA = ' . $donnees_formation['ID_ORGA']);
                                $donnees_organisme = $requeteSql_organisme->fetch();
                                echo '</div>';
                                echo '</div>';
                                echo '</div>';
                                $requeteSql_description->closeCursor();
                                $requeteSql_modalite->closeCursor();
                                $requeteSql_organisme->closeCursor();
                            }
                        }
                        $requeteSql_formation->closeCursor();
                    }
                }else if($count_prix != 0){
                    $conditions = '';
                    for ($i = 0; $i < $count_prix; $i++) {
                        if($i==0 && $count_prix < 2){
                            switch($f_nom_prix[$i]){
                                case $tab_prix[0] : $conditions = ' ( PRIX < 100.00 )';break;
                                case $tab_prix[1] : $conditions = ' ( PRIX > 100.00 AND PRIX < 200.00 )';break;
                                case $tab_prix[2] : $conditions = ' ( PRIX > 200.00 AND PRIX < 500.00 )';break;
                                case $tab_prix[3] : $conditions = ' ( PRIX > 500.00 )';break;
                            }
                        } else if($i==0){
                            switch($f_nom_prix[$i]){
                                case $tab_prix[0] : $conditions = ' ( PRIX < 100.00 )';break;
                                case $tab_prix[1] : $conditions = ' ( PRIX > 100.00 AND PRIX < 200.00 )';break;
                                case $tab_prix[2] : $conditions = ' ( PRIX > 200.00 AND PRIX < 500.00 )';break;
                                case $tab_prix[3] : $conditions = ' ( PRIX > 500.00 )';break;
                            }
                        } else if($i==$count){
                            switch($f_nom_prix[$i]){
                                case $tab_prix[0] : $conditions .= ' OR ( PRIX < 100.00 )';break;
                                case $tab_prix[1] : $conditions .= ' OR ( PRIX > 100.00 AND PRIX < 200.00 )';break;
                                case $tab_prix[2] : $conditions .= ' OR ( PRIX > 200.00 AND PRIX < 500.00 )';break;
                                case $tab_prix[3] : $conditions .= ' OR ( PRIX > 500.00 )';break;
                            }
                        } else {
                            switch($f_nom_prix[$i]){
                                case $tab_prix[0] : $conditions .= ' OR ( PRIX < 100.00 )';break;
                                case $tab_prix[1] : $conditions .= ' OR ( PRIX > 100.00 AND PRIX < 200.00 )';break;
                                case $tab_prix[2] : $conditions .= ' OR ( PRIX > 200.00 AND PRIX < 500.00 )';break;
                                case $tab_prix[3] : $conditions .= ' OR ( PRIX > 500.00 )';break;
                            }
                        }
                    }
                    $domaine='';
                    if($f_nom_domaine!='') {
                        $requeteID_domaine2 = $bdd->query('SELECT * FROM at_domaine2 WHERE NOM_D2 LIKE "%' . $f_nom_domaine . '%"');
                        $donnees_domaine2 = $requeteID_domaine2->fetch();
                        $domaine = 'AND ID_D2='.$donnees_domaine2['ID_D2'];
                    }
                    if (!isset($_POST['name']) || $_POST['name']==null || $_POST['name']=='') {
                        echo '<div class="container">';
                        $requete_formation = 'SELECT * FROM t_formation WHERE'.$conditions.' '.$domaine;
                        $requeteSql_formation = $bdd->query($requete_formation);
                        while ($donnees_formation = $requeteSql_formation->fetch()) {
                            echo '<div class="row">';
                            echo '<div class="container article">';
                            echo '<div class="row align-items-center">';
                            echo '<div class="col-2">';
                            echo '<img src="../images/Informatique.jpg" width="100%" class="photo"></div>';
                            echo '<div class="col">';
                            echo '<h3 class="title">' . $donnees_formation['TITRE'] . '</h3></div>';
                            echo '<div class="col btn-2">';
                            echo '<a href="fiche-formation.php?numero=' . $donnees_formation['ID_FORM'] . '&nom=' . $donnees_formation['TITRE'] . '" target="_blank" class="button-2"><i class="fas fa-plus"></i><span>&nbsp;Plus d\'info</span></a></div></div>';
                            echo '<div class="row">';
                            echo '<div class="col-lg-12 desc">';
                            $requeteSql_description = $bdd->prepare('SELECT * FROM t_description WHERE ID_DESCRIPTION = ?');
                            $requeteSql_description->execute(array($donnees_formation['ID_DESCRIPTION']));
                            $donnees_description = $requeteSql_description->fetch();
                            echo '<h4 class="overflow-ellipsis; overflow-visible;">' . $donnees_description['CHAPEAU'] . '</h4>';
                            $requeteSql_description->closeCursor();
                            echo '</div></div>';
                            echo '<div class="row align-items-center overlay">';
                            echo '<div class="col-3 ">';
                            echo '<h5 class="li">' . $donnees_formation['DUREE'] . '</h5></div>';
                            $requeteSql_modalite = $bdd->query('SELECT * FROM at_modalite WHERE ID_MOD =' . $donnees_formation['ID_MOD']);
                            $donnees_modalite = $requeteSql_modalite->fetch();
                            echo '<div class="col-3 mx-auto"><h5 class="li">' . $donnees_modalite['NOM_MOD'] . '</h5></div>';
                            $requeteSql_modalite->closeCursor();
                            echo '<div class="col-3 mx-auto"><h5 class="li">' . $donnees_formation['PRIX'] . '€</h5></div>';
                            $requeteSql_organisme = $bdd->query('SELECT * FROM t_organisation WHERE ID_ORGA = ' . $donnees_formation['ID_ORGA']);
                            $donnees_organisme = $requeteSql_organisme->fetch();
                            echo '</div>';
                            echo '</div>';
                            echo '</div>';
                            $requeteSql_description->closeCursor();
                            $requeteSql_modalite->closeCursor();
                            $requeteSql_organisme->closeCursor();
                        }
                        $requeteSql_formation->closeCursor();
                    } else {
                        echo '<div class="container">';
                        $requete_formation = 'SELECT * FROM t_formation WHERE'.$conditions.' AND TITRE LIKE "%'. $_POST['name'] . '%" '.$domaine;
                        $requeteSql_formation = $bdd->query($requete_formation);
                        while ($donnees_formation = $requeteSql_formation->fetch()) {
                            echo '<div class="row">';
                            echo '<div class="container article">';
                            echo '<div class="row align-items-center">';
                            echo '<div class="col-2">';
                            echo '<img src="../images/Informatique.jpg" width="100%" class="photo"></div>';
                            echo '<div class="col">';
                            echo '<h3 class="title">' . $donnees_formation['TITRE'] . '</h3></div>';
                            echo '<div class="col btn-2">';
                            echo '<a href="fiche-formation.php?numero=' . $donnees_formation['ID_FORM'] . '&nom=' . $donnees_formation['TITRE'] . '" target="_blank" class="button-2"><i class="fas fa-plus"></i><span>&nbsp;Plus d\'info</span></a></div></div>';
                            echo '<div class="row">';
                            echo '<div class="col-lg-12 desc">';
                            $requeteSql_description = $bdd->prepare('SELECT * FROM t_description WHERE ID_DESCRIPTION = ?');
                            $requeteSql_description->execute(array($donnees_formation['ID_DESCRIPTION']));
                            $donnees_description = $requeteSql_description->fetch();
                            echo '<h4 class="overflow-ellipsis; overflow-visible;">' . $donnees_description['CHAPEAU'] . '</h4>';
                            $requeteSql_description->closeCursor();
                            echo '</div></div>';
                            echo '<div class="row align-items-center overlay">';
                            echo '<div class="col-3 ">';
                            echo '<h5 class="li">' . $donnees_formation['DUREE'] . '</h5></div>';
                            $requeteSql_modalite = $bdd->query('SELECT * FROM at_modalite WHERE ID_MOD =' . $donnees_formation['ID_MOD']);
                            $donnees_modalite = $requeteSql_modalite->fetch();
                            echo '<div class="col-3 mx-auto"><h5 class="li">' . $donnees_modalite['NOM_MOD'] . '</h5></div>';
                            $requeteSql_modalite->closeCursor();
                            echo '<div class="col-3 mx-auto"><h5 class="li">' . $donnees_formation['PRIX'] . '€</h5></div>';
                            $requeteSql_organisme = $bdd->query('SELECT * FROM t_organisation WHERE ID_ORGA = ' . $donnees_formation['ID_ORGA']);
                            $donnees_organisme = $requeteSql_organisme->fetch();
                            echo '</div>';
                            echo '</div>';
                            echo '</div>';
                            $requeteSql_description->closeCursor();
                            $requeteSql_modalite->closeCursor();
                            $requeteSql_organisme->closeCursor();
                        }
                        $requeteSql_formation->closeCursor();
                    }
                }else if($f_nom_domaine!=''){
                    $requeteID_domaine2 = $bdd->query('SELECT * FROM at_domaine2 WHERE NOM_D2 LIKE "%'.$f_nom_domaine.'%"');
                    $donnees_domaine2 = $requeteID_domaine2->fetch();
                    if (!isset($_POST['name'])) {
                        echo '<div class="container">';
                        $requeteSql_formation = $bdd->query('SELECT * FROM t_formation WHERE ID_D2='.$donnees_domaine2['ID_D2']);
                        while ($donnees_formation = $requeteSql_formation->fetch()) {
                            echo '<div class="row">';
                            echo '<div class="container article">';
                            echo '<div class="row align-items-center">';
                            echo '<div class="col-2">';
                            echo '<img src="../images/Informatique.jpg" width="100%" class="photo"></div>';
                            echo '<div class="col">';
                            echo '<h3 class="title">' . $donnees_formation['TITRE'] . '</h3></div>';
                            echo '<div class="col btn-2">';
                            echo '<a href="fiche-formation.php?numero=' . $donnees_formation['ID_FORM'] . '&nom=' . $donnees_formation['TITRE'] . '" target="_blank" class="button-2"><i class="fas fa-plus"></i><span>&nbsp;Plus d\'info</span></a></div></div>';
                            echo '<div class="row">';
                            echo '<div class="col-lg-12 desc">';
                            $requeteSql_description = $bdd->prepare('SELECT * FROM t_description WHERE ID_DESCRIPTION = ?');
                            $requeteSql_description->execute(array($donnees_formation['ID_DESCRIPTION']));
                            $donnees_description = $requeteSql_description->fetch();
                            echo '<h4 class="overflow-ellipsis; overflow-visible;">' . $donnees_description['CHAPEAU'] . '</h4>';
                            $requeteSql_description->closeCursor();
                            echo '</div></div>';
                            echo '<div class="row align-items-center overlay">';
                            echo '<div class="col-3 ">';
                            echo '<h5 class="li">' . $donnees_formation['DUREE'] . '</h5></div>';
                            $requeteSql_modalite = $bdd->query('SELECT * FROM at_modalite WHERE ID_MOD =' . $donnees_formation['ID_MOD']);
                            $donnees_modalite = $requeteSql_modalite->fetch();
                            echo '<div class="col-3 mx-auto"><h5 class="li">' . $donnees_modalite['NOM_MOD'] . '</h5></div>';
                            $requeteSql_modalite->closeCursor();
                            echo '<div class="col-3 mx-auto"><h5 class="li">' . $donnees_formation['PRIX'] . '€</h5></div>';
                            $requeteSql_organisme = $bdd->query('SELECT * FROM t_organisation WHERE ID_ORGA = ' . $donnees_formation['ID_ORGA']);
                            $donnees_organisme = $requeteSql_organisme->fetch();
                            echo '</div>';
                            echo '</div>';
                            echo '</div>';
                            $requeteSql_description->closeCursor();
                            $requeteSql_modalite->closeCursor();
                            $requeteSql_organisme->closeCursor();
                        }
                        $requeteSql_formation->closeCursor();
                    } else {
                        echo '<div class="container">';
                        $requeteSql_formation = $bdd->query('SELECT * FROM t_formation WHERE TITRE LIKE "%' . $_POST['name'] . '%" AND ID_D2='.$donnees_domaine2['ID_D2']);
                        while ($donnees_formation = $requeteSql_formation->fetch()) {
                            echo '<div class="row">';
                            echo '<div class="container article">';
                            echo '<div class="row align-items-center">';
                            echo '<div class="col-2">';
                            echo '<img src="../images/Informatique.jpg" width="100%" class="photo"></div>';
                            echo '<div class="col">';
                            echo '<h3 class="title">' . $donnees_formation['TITRE'] . '</h3></div>';
                            echo '<div class="col btn-2">';
                            echo '<a href="fiche-formation.php?numero=' . $donnees_formation['ID_FORM'] . '&nom=' . $donnees_formation['TITRE'] . '" target="_blank" class="button-2"><i class="fas fa-plus"></i><span>&nbsp;Plus d\'info</span></a></div></div>';
                            echo '<div class="row">';
                            echo '<div class="col-lg-12 desc">';
                            $requeteSql_description = $bdd->prepare('SELECT * FROM t_description WHERE ID_DESCRIPTION = ?');
                            $requeteSql_description->execute(array($donnees_formation['ID_DESCRIPTION']));
                            $donnees_description = $requeteSql_description->fetch();
                            echo '<h4 class="overflow-ellipsis; overflow-visible;">' . $donnees_description['CHAPEAU'] . '</h4>';
                            $requeteSql_description->closeCursor();
                            echo '</div></div>';
                            echo '<div class="row align-items-center overlay">';
                            echo '<div class="col-3 ">';
                            echo '<h5 class="li">' . $donnees_formation['DUREE'] . '</h5></div>';
                            $requeteSql_modalite = $bdd->query('SELECT * FROM at_modalite WHERE ID_MOD =' . $donnees_formation['ID_MOD']);
                            $donnees_modalite = $requeteSql_modalite->fetch();
                            echo '<div class="col-3 mx-auto"><h5 class="li">' . $donnees_modalite['NOM_MOD'] . '</h5></div>';
                            $requeteSql_modalite->closeCursor();
                            echo '<div class="col-3 mx-auto"><h5 class="li">' . $donnees_formation['PRIX'] . '€</h5></div>';
                            $requeteSql_organisme = $bdd->query('SELECT * FROM t_organisation WHERE ID_ORGA = ' . $donnees_formation['ID_ORGA']);
                            $donnees_organisme = $requeteSql_organisme->fetch();
                            //echo '<div class="col-3 mx-auto"><h5 class="li">' . $donnees_organisme['NOM_ORGA'] . '</h5>';
                            //echo '</div>';
                            echo '</div>';
                            echo '</div>';
                            echo '</div>';
                            $requeteSql_description->closeCursor();
                            $requeteSql_modalite->closeCursor();
                            $requeteSql_organisme->closeCursor();
                        }
                        $requeteSql_formation->closeCursor();
                    }
                }else {
                    if (!isset($_POST['name'])) {
                        echo '<div class="container">';
                        $requeteSql_formation = $bdd->query('SELECT * FROM t_formation');
                        while ($donnees_formation = $requeteSql_formation->fetch()) {
                            echo '<div class="row">';
                            echo '<div class="container article">';
                            echo '<div class="row align-items-center">';
                            echo '<div class="col-2">';
                            echo '<img src="../images/Informatique.jpg" width="100%" class="photo"></div>';
                            echo '<div class="col">';
                            echo '<h3 class="title">' . $donnees_formation['TITRE'] . '</h3></div>';
                            echo '<div class="col btn-2">';
                            echo '<a href="fiche-formation.php?numero=' . $donnees_formation['ID_FORM'] . '&nom=' . $donnees_formation['TITRE'] . '" target="_blank" class="button-2"><i class="fas fa-plus"></i><span>&nbsp;Plus d\'info</span></a></div></div>';
                            echo '<div class="row">';
                            echo '<div class="col-lg-12 desc">';
                            $requeteSql_description = $bdd->prepare('SELECT * FROM t_description WHERE ID_DESCRIPTION = ?');
                            $requeteSql_description->execute(array($donnees_formation['ID_DESCRIPTION']));
                            $donnees_description = $requeteSql_description->fetch();
                            echo '<h4 class="overflow-ellipsis; overflow-visible;">' . $donnees_description['CHAPEAU'] . '</h4>';
                            $requeteSql_description->closeCursor();
                            echo '</div></div>';
                            echo '<div class="row align-items-center overlay">';
                            echo '<div class="col-3 ">';
                            echo '<h5 class="li">' . $donnees_formation['DUREE'] . '</h5></div>';
                            $requeteSql_modalite = $bdd->query('SELECT * FROM at_modalite WHERE ID_MOD =' . $donnees_formation['ID_MOD']);
                            $donnees_modalite = $requeteSql_modalite->fetch();
                            echo '<div class="col-3 mx-auto"><h5 class="li">' . $donnees_modalite['NOM_MOD'] . '</h5></div>';
                            $requeteSql_modalite->closeCursor();
                            echo '<div class="col-3 mx-auto"><h5 class="li">' . $donnees_formation['PRIX'] . '€</h5></div>';
                            $requeteSql_organisme = $bdd->query('SELECT * FROM t_organisation WHERE ID_ORGA = ' . $donnees_formation['ID_ORGA']);
                            $donnees_organisme = $requeteSql_organisme->fetch();
                            echo '</div>';
                            echo '</div>';
                            echo '</div>';
                            $requeteSql_description->closeCursor();
                            $requeteSql_modalite->closeCursor();
                            $requeteSql_organisme->closeCursor();
                        }
                        $requeteSql_formation->closeCursor();
                    } else {
                        echo '<div class="container">';
                        $requeteSql_formation = $bdd->query('SELECT * FROM t_formation WHERE TITRE LIKE "%' . $_POST['name'] . '%"');
                        while ($donnees_formation = $requeteSql_formation->fetch()) {
                            echo '<div class="row">';
                            echo '<div class="container article">';
                            echo '<div class="row align-items-center">';
                            echo '<div class="col-2">';
                            echo '<img src="../images/Informatique.jpg" width="100%" class="photo"></div>';
                            echo '<div class="col">';
                            echo '<h3 class="title">' . $donnees_formation['TITRE'] . '</h3></div>';
                            echo '<div class="col btn-2">';
                            echo '<a href="fiche-formation.php?numero=' . $donnees_formation['ID_FORM'] . '&nom=' . $donnees_formation['TITRE'] . '" target="_blank" class="button-2"><i class="fas fa-plus"></i><span>&nbsp;Plus d\'info</span></a></div></div>';
                            echo '<div class="row">';
                            echo '<div class="col-lg-12 desc">';
                            $requeteSql_description = $bdd->prepare('SELECT * FROM t_description WHERE ID_DESCRIPTION = ?');
                            $requeteSql_description->execute(array($donnees_formation['ID_DESCRIPTION']));
                            $donnees_description = $requeteSql_description->fetch();
                            echo '<h4 class="overflow-ellipsis; overflow-visible;">' . $donnees_description['CHAPEAU'] . '</h4>';
                            $requeteSql_description->closeCursor();
                            echo '</div></div>';
                            echo '<div class="row align-items-center overlay">';
                            echo '<div class="col-3 ">';
                            echo '<h5 class="li">' . $donnees_formation['DUREE'] . '</h5></div>';
                            $requeteSql_modalite = $bdd->query('SELECT * FROM at_modalite WHERE ID_MOD =' . $donnees_formation['ID_MOD']);
                            $donnees_modalite = $requeteSql_modalite->fetch();
                            echo '<div class="col-3 mx-auto"><h5 class="li">' . $donnees_modalite['NOM_MOD'] . '</h5></div>';
                            $requeteSql_modalite->closeCursor();
                            echo '<div class="col-3 mx-auto"><h5 class="li">' . $donnees_formation['PRIX'] . '€</h5></div>';
                            $requeteSql_organisme = $bdd->query('SELECT * FROM t_organisation WHERE ID_ORGA = ' . $donnees_formation['ID_ORGA']);
                            $donnees_organisme = $requeteSql_organisme->fetch();
                            //echo '<div class="col-3 mx-auto"><h5 class="li">' . $donnees_organisme['NOM_ORGA'] . '</h5>';
                            //echo '</div>';
                            echo '</div>';
                            echo '</div>';
                            echo '</div>';
                            $requeteSql_description->closeCursor();
                            $requeteSql_modalite->closeCursor();
                            $requeteSql_organisme->closeCursor();
                        }
                        $requeteSql_formation->closeCursor();
                    }
                }
                ?>
            </div>
        </div>
    </div>
</div>
</div>


<footer>
    <div class="container">
        <div class="row m-auto">
            <div class="col-md-7 m-auto archi">
                <div class="row justify-content-between grand-titre">
                    <div class="col"><h3>Redirection</h3></div>
                    <div class="col"><h3>Préférences</h3></div>
                    <div class="col"><h3>Support</h3></div>
                </div>
                <div class="row justify-content-between">
                    <div class="col"><a href="../index.php"><p class="texte">Accueil</p></a></div>
                    <div class="col"><p class="texte">Web</p></div>
                    <div class="col"><p class="texte"><a href="Contacts.php" style="color:white">Contact</a></p></div>
                </div>
                <div class="row justify-content-between">
                    <div class="col"><a href="Recherche.php"><p class="texte">Recherche</p></a></div>
                    <div class="col"><p class="texte">Marketing</p></div>
                    <div class="col"><p class="texte"></p></div>
                </div>
                <div class="row justify-content-between">
                    <div class="col"><a href="#"><p class="texte">Organisme</p></a></div>
                    <div class="col"><p class="texte"></p></div>
                    <div class="col"><p class="texte"></p></div>
                </div>
            </div>
            <div class="col-md-3 PR m-auto">
                <table>
                    <tr><td><a href="https://www.facebook.com/treco.tech/" target="_blank"><img src="../images/fb.png" width="24px"/></a></td></tr>
                    <tr><td><a href="https://fr.linkedin.com/company/treco-tech" target="_blank"><img src="../images/lkd.png" width="24px"/></a></td></tr>
                    <tr><td><a href="https://twitter.com/treco_tech" target="_blank"><img src="../images/tw.jpg" width="24px"/></a></td></tr>
                    <tr><td><a href="https://treco.tech" target="_blank"><img src="../images/Bl.png" width="24px"/></a></td></tr>
                </table>
                <img id="img_footer" src="../images/TRECO-white.png" width="200px" height="65px"/>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="copyright">Copyright © 2018. Tous droits réservés.</div>
        </div>
    </div>
</footer>

<?php $bdd = null;?>
</body>

</html>