<?php session_start(); 
  include("Connexion.php");
  if(!isset($_SESSION['etat'])){$_SESSION['etat']="deconnecte";}
  if(!isset($_SESSION['type'])){$_SESSION['type']=null;}
  if(!isset($_SESSION['id'])){$_SESSION['id']=null;}
  if(!isset($_GET['modificaion'])){$_SESSION['modification']=false;}
  //if($_SESSION['id']==null){header('Location: index.php');}
  $_SESSION['page_actuelle']="ajouter-formation.php";
  ?>

<!DOCTYPE html>
<html>
   <head>
      <meta charset="utf-8">
      <link href="../style/ajouter-formation.css" rel="stylesheet">
      <title>TRECO</title>
   </head>
   <body>
      <header>
         <div class="wrapper">
         <div>
            <img src="../images/Logo-TRECO.png"> 
            <nav>
               <ul>
                  <li><a href="index.php">Accueil</a></li>
                  <li><a href="Contacts.php">Contacts</a></li>
                  <?php
                  if($_SESSION['id']!=null){
                      echo '<li><a href="deconnexion.php">Deconnexion</a></li>';
                  }else{
                      echo '<li><a href="login.php">Se connecter</a></li>';
                  }
                  ?>
                  <li><a href="Recherche.php">Rechercher</a></li>
               </ul>
            </nav>
         </div>
      </header>
      <section id="main-image">
         <div class="wrapper">
            <h2>AJOUTER UNE FORMATION</h2>
         </div>
      </section>
      <?php 
      /*Message d'erreur reçu de la vérification si elle est existante et apparait aux yeux de l'utilisateurs*/
        if(isset($_SESSION['messageErreur'])){
          if($_SESSION['messageErreur']!=null){
            echo "<br /><center><message id=\"erreur\">".$_SESSION['messageErreur']."</message></center><br />";
          }
        }
      ?>
      <section id="contenu">
         <div class="wrapper">
             <?php if (!isset($_GET['formulaire'])){?>
                <form method="post" action="verification_formation.php">
                   <p>
                      <label for="titre">Titre de la formation (30 caractères)</label><br />
                      <textarea name="titre" id="titre" rows="1" cols="70"></textarea>
                   </p>
                   <p>
                      <label for="lien">Lien du site</label><br />
                      <textarea name="lien" id="titre" rows="1" cols="70"></textarea>
                   </p>
                   <p>
                      <label for="presentation">Présentation (120 caractères)</label><br />
                      <textarea name="presentation" id="presentation" rows="2" cols="100"></textarea>
                   </p>
                   <p>
                      <label for="objectifs">Objectifs de la formation</label><br />
                      <textarea name="objectifs" id="objectifs" rows="3" cols="100"></textarea>
                   </p>
                   <p>
                      <label for="programme">Programme</label><br />
                      <textarea name="programme" id="programme" rows="60" cols="100"></textarea>
                   </p>
                   <p>
                      <label for="prerequis">Prérequis</label><br />
                      <textarea name="prerequis" id="prerequis" rows="3" cols="100"></textarea>
                   </p>
                   <p>
                      <label for="public">Public</label><br />
                      <textarea name="public" id="public" rows="3" cols="100"></textarea>
                   </p>
                   <li>Domaine de compétence principal</li>
                   <SELECT id="domaine1" name="domaine_1" size="1">
                      <?php
                        $rechercheDomaine = $bdd->query('SELECT * FROM at_domaine1');

                        while($donnees = $rechercheDomaine->fetch())
                        {
                          echo '<option ';
                          if('Développement informatique'==$donnees['NOM_D1'])
                             {
                              echo 'selected="selected" ';
                             }
                          echo 'value="'.$donnees['NOM_D1'].'">'.$donnees['NOM_D1'].'</option>' ;
                        }
                        $rechercheDomaine->closeCursor();
                      ?>
                   </SELECT>
                   <br>

                   <li>Domaine de compétence secondaire</li>
                   <SELECT name="domaine_2" size="1">
                       <?php
                            $recherche_domaine2 = $bdd->query('SELECT * FROM at_domaine2');

                            while($donnees = $recherche_domaine2->fetch()){
                                echo '<option ';
                                if('App, mobile'==$donnees['NOM_D2']){
                                    echo 'selected="selected" ';
                                }
                                echo 'value="'.$donnees['NOM_D2'].'">'.$donnees['NOM_D2'].'</option>';
                            }
                            $recherche_domaine2->closeCursor();
                       ?>
                   </SELECT>
                   <br>
                   <li>Modalité pédagogique</li>
                   <SELECT name="modalite" size="1">
                      <?php
                        $rechercheModalite = $bdd->query('SELECT * FROM at_modalite');

                        while($donnees = $rechercheModalite->fetch())
                        {
                          echo '<option ';
                          if('E-learning'==$donnees['NOM_MOD'])
                             {
                              echo 'selected="selected" ';
                             }
                          echo 'value="'.$donnees['NOM_MOD'].'">'.$donnees['NOM_MOD'].'</option>' ;
                        }
                        $rechercheModalite->closeCursor();
                      ?>
                   </SELECT>
                   <br><br>
                      <p>
                         <label for="titre">Prix HT</label><br />
                         <textarea type="number" name="HT" id="HT" rows="1" cols="10"></textarea>
                      </p>
                      <p>
                         <label for="TTC">Prix TTC</label><br />
                         <textarea type="numer" name="TTC" id="TTC" rows="1" cols="10"></textarea>
                      </p>
                      <p>
                         <label for="duree">Duree</label><br />
                         <textarea name="duree" id="public" rows="3" cols="100"></textarea>
                      </p>
                      <p>
                         <label for="TTC">Adresse de la formation</label><br />
                         <textarea name="adresse" id="adresse" rows="3" cols="50"></textarea>
                      </p>
                      <center><input type="Submit" value="Valider"/></center>
                </form>
             <?php } else {
                 $_SESSION['page_actuelle']="ajouter-formation.php?formulaire=".$_GET['formulaire'];
                 $requeteSql_formation = $bdd->query('SELECT * FROM t_formation WHERE ID_FORM = '.$_GET['formulaire']);
                 $donnees_formation = $requeteSql_formation->fetch();
                 $requeteSql_description = $bdd->query('SELECT * FROM t_description WHERE ID_DESCRIPTION = '.$donnees_formation['ID_DESCRIPTION']);
                 $donnees_description = $requeteSql_description->fetch();
                 $requeteSql_adresse = $bdd->query('SELECT * FROM t_adresse WHERE ID_ADR = '.$donnees_formation['ID_ADR']);
                 $donnees_adresse = $requeteSql_adresse->fetch();
                 $requeteSql_domaine2bis = $bdd->query('SELECT * FROM at_domaine2 WHERE ID_D2 = '.$donnees_formation['ID_D2']);
                 $donnees_domaine2bis = $requeteSql_domaine2bis->fetch();
                 echo '<form method="post" action="verification_formation.php?formulaire='.$_GET['formulaire'].'&amp;modification=true"><p>';
                 echo '<label for="titre">Titre de la formation (30 caractères)</label><br />';
                 echo '<textarea name="titre" id="titre" rows="1" cols="70">'.$donnees_formation['TITRE'].'</textarea></p>';
                 echo '<p><label for="presentation">Présentation (120 caractères)</label><br />';
                 echo '<textarea name="presentation" id="presentation" rows="2" cols="100">'.$donnees_description['CHAPEAU'].'</textarea></p>';
                 echo '<p><label for="objectifs">Objectifs de la formation</label><br />';
                 echo '<textarea name="objectifs" id="objectifs" rows="3" cols="100">'.$donnees_description['OBJECTIF'].'</textarea></p>';
                 echo '<p><label for="programme">Programme</label><br />';
                 echo '<textarea name="programme" id="programme" rows="60" cols="100"></textarea></p>';
                 echo '<p><label for="prerequis">Prérequis</label><br />';
                 echo '<textarea name="prerequis" id="prerequis" rows="3" cols="100">'.$donnees_description['PRE_REQUI'].'</textarea></p>';
                 echo '<p><label for="public">Public</label><br />';
                 echo '<textarea name="public" id="public" rows="3" cols="100">'.$donnees_description['PUBLIC_CIBLE'].'</textarea></p>';
                 echo '<li>Domaine de compétence principal</li>';
                 echo '<SELECT id="domaine1" name="domaine_1" size="1">';
                 $rechercheDomaine = $bdd->query('SELECT * FROM at_domaine1');

                 while($donnees = $rechercheDomaine->fetch())
                 {
                     echo '<option ';
                     if($donnees_domaine2bis['ID_D1']==$donnees['ID_D1'])
                     {
                         echo 'selected="selected" ';
                     }
                     echo 'value="'.$donnees['NOM_D1'].'">'.$donnees['NOM_D1'].'</option>' ;
                 }
                 $rechercheDomaine->closeCursor();
                 echo '</SELECT><br />';
                 echo '<li>Domaine de compétence secondaire</li><SELECT name="domaine_2" size="1">';
                 $recherche_domaine2 = $bdd->query('SELECT * FROM at_domaine2');

                 while($donnees = $recherche_domaine2->fetch()){
                     echo '<option ';
                     if($donnees_domaine2bis['NOM_D2']==$donnees['NOM_D2']){
                         echo 'selected="selected" ';
                     }
                     echo 'value="'.$donnees['NOM_D2'].'">'.$donnees['NOM_D2'].'</option>';
                 }
                 $recherche_domaine2->closeCursor();
                 echo '</SELECT><br /><li>Modalité pédagogique</li>';
                 echo '<SELECT name="modalite" size="1">';
                 $rechercheModalite = $bdd->query('SELECT * FROM at_modalite');

                 while($donnees = $rechercheModalite->fetch())
                 {
                     echo '<option ';
                     if($donnees_formation['ID_MOD']==$donnees['ID_MOD'])
                     {
                         echo 'selected="selected" ';
                     }
                     echo 'value="'.$donnees['NOM_MOD'].'">'.$donnees['NOM_MOD'].'</option>' ;
                 }
                 $rechercheModalite->closeCursor();
                 $requeteSql_type_voix = $bdd->query('SELECT * FROM at_type_voix WHERE ID_TV = '.$donnees_adresse['ID_TV']);
                 $donnees_type_voix = $requeteSql_type_voix->fetch();
                 echo '</SELECT><br><br>';
                 echo ' <p><label for="titre">Prix HT</label><br />';
                 echo '<textarea type="number" name="HT" id="HT" rows="1" cols="10">'.$donnees_formation['PRIX']*0.8.'</textarea></p>';
                 echo '<p><label for="TTC">Prix TTC</label><br />';
                 echo '<textarea type="numer" name="TTC" id="TTC" rows="1" cols="10">'.$donnees_formation['PRIX'].'</textarea></p>';
                 echo '<p><label for="TTC">Adresse de la formation</label><br />';
                 echo '<textarea name="adresse" id="adresse" rows="3" cols="50">'.$donnees_adresse['NUMERO'].' '.$donnees_type_voix['NOM_TV'].' '.$donnees_adresse['NOM_ADR'].'</textarea></p>';
                 echo '<center><input type="button" value="Annuler" onclick="javascript:location.href=\'catalogue.php\'"/><input type="Submit" value="Valider"/></center>
                 </form>';
             }?>
         </div>
      </section>
      <footer>
      <div class="wrapper">
      <center><img src="../images/TRECO-white.png"></center>
      <div class="copyright">Copyright © 2018. Tous droits réservés.</div>
      </div>
      </footer>     
   </body>
</html>