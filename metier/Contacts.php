<?php
    session_start();
    if(!isset($_SESSION['etat'])){$_SESSION['etat']="deconnecte";}
    if(!isset($_SESSION['type'])){$_SESSION['type']=null;}
    if(!isset($_SESSION['id'])){$_SESSION['id']=null;}
    if(!isset($_SESSION['message'])){$_SESSION['message']=null;}
    $_SESSION['page_actuelle']="Contacts.php";
    ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width" />
        <link href="../style/bootstrap.css" rel="stylesheet"/>
        <link href="../style/Contacts.css" rel="stylesheet" />
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
        <link rel="shortcut icon" type="image/x-icon" href="../images/favicon.ico" />
        <title>Treco : posez-nous vos questions</title>
            <!-- Global site tag (gtag.js) - Google Analytics -->
            <script async src="https://www.googletagmanager.com/gtag/js?id=UA-119675621-1"></script>
            <script>
               window.dataLayer = window.dataLayer || [];
               function gtag(){dataLayer.push(arguments);}
               gtag('js', new Date());

               gtag('config', 'UA-119675621-1');
            </script>
            <script type="text/javascript"> _linkedin_data_partner_id = "374204"; </script><script type="text/javascript"> (function(){var s = document.getElementsByTagName("script")[0]; var b = document.createElement("script"); b.type = "text/javascript";b.async = true; b.src = "https://snap.licdn.com/li.lms-analytics/insight.min.js"; s.parentNode.insertBefore(b, s);})(); </script> <noscript> <img height="1" width="1" style="display:none;" alt="" src="https://dc.ads.linkedin.com/collect/?pid=374204&fmt=gif
https://dc.ads.linkedin.com/collect/?pid=374204&fmt=gif
" /> </noscript>
    </head>

    <header class="site-header">
        <div class="container">
            <div class="row" >
                <div class="col-lg-3">
                    <a href="../index.php"><img id="img_tete" src="../images/TRECO-white.png" width="60%"/></a>
                </div>
                <div class="row col-lg-6 mx-auto rechercher">
                    <form method="get" action="Recherche.php" class="recherche">
                        <div class="row justify-content-between" width="100%">
                            <div class="col-5">
                                <input id="fonctionR" type="text" name="name" placeholder="Entrez votre recherche" />
                            </div>
                            <div class="col-1">
                                <button type="submit" class="btn btn-primary button-R"><i class="fas fa-search"></i></button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="row col-lg-3 mx-auto navigation">
                    <div class="col-lg-1 my-auto ">
                        <a id="menu" href="../index.php">Accueil</a>
                    </div>
                    <div class="col-lg-1 my-auto mx-auto">
                        <a id="menu" href="organisme-de-formation.php">Organisme</a>
                    </div>
                    <div class="col-lg-1 my-auto">
                        <a id="menu" href="Contacts.php">Contacts</a>
                    </div>
                </div>
            </div>
        </div>
    </header>
    
    <body>            
        <div id="contacts">
	        <div class="wrapper">
                <h3 class="titre">Vous pouvez nous contacter via ce formulaire</h3>
            </div>
	    </div>

        <div class="container formulaire">
                    <form method="post" action="envoie_mail.php">
                        <div class="row prenomC">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="">Prenom</span>
                                </div>
                                <input type="text" name="prenom" class="form-control"/>
                            </div>
                        </div>
                        <div class="row nomC">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="">Nom</span>
                                </div>
                                <input type="text" name="nom" class="form-control"/>
                            </div>
                        </div>
                        <div class="row mail">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">@</span>
                                </div>
                                <input type="text" name="email" class="form-control"/>
                            </div>
                        </div>
                        <div class="row message">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Message</span>
                                </div>
                                <textarea class="form-control" name="message"></textarea>
                            </div>
                        </div>
                        <div class="row conditions">
                            <h4>En acceptant de remplir ce formulaire et de cocher la case ci-dessous, vous nous permettez de vous répondre en connaissance de votre identité</h4>
                        </div>
                        <div class="row check">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" name="terme" class="custom-control-input" id="customControlValidation1" required>
                                <label class="custom-control-label" for="customControlValidation1"></label>
                            </div>
                        </div>
                        <div class="row boutonE">
                            <input class="bouton btn btn-primary" type="submit" value="Envoyer"/>
                        </div>
                    </form>
                </div>
        </div>

        <footer>
            <div class="container">
                <div class="row m-auto">
                    <div class="col-lg-7 m-auto archi">
                        <div class="row justify-content-between grand-titre">
                            <div class="col"><h3>Redirection</h3></div>
                            <div class="col"><h3>Préférences</h3></div>
                            <div class="col"><h3>Support</h3></div>
                        </div>
                        <div class="row justify-content-between">
                            <div class="col"><a href="../index.php"><p class="texte">Accueil</p></a></div>
                            <div class="col"><p class="texte">Web</p></div>
                            <div class="col"><p class="texte"><a href="Contacts.php" style="color:white">Contact</a></p></div>
                        </div>
                        <div class="row justify-content-between">
                            <div class="col"><a href="Recherche.php"><p class="texte">Recherche</p></a></div>
                            <div class="col"><p class="texte">Marketing</p></div>
                            <div class="col"><p class="texte"></p></div>
                        </div>
                        <div class="row justify-content-between">
                            <div class="col"><a href="#"><p class="texte">Organisme</p></a></div>
                            <div class="col"><p class="texte"></p></div>
                            <div class="col"><p class="texte"></p></div>
                        </div>
                    </div>
                    <div class="col-lg-3 PR m-auto">
                        <table>
                            <tr><td><a href="https://www.facebook.com/treco.tech/" target="_blank"><img src="../images/fb.png" width="24px"/></a></td></tr>
                            <tr><td><a href="https://fr.linkedin.com/company/treco-tech" target="_blank"><img src="../images/lkd.png" width="24px"/></a></td></tr>
                            <tr><td><a href="https://twitter.com/treco_tech" target="_blank"><img src="../images/tw.jpg" width="24px"/></a></td></tr>
                            <tr><td><a href="https://treco.tech" target="_blank"><img src="../images/Bl.png" width="24px"/></a></td></tr>
                        </table>
                        <img id="img_footer" src="../images/TRECO-white.png" width="200px" height="65px"/>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="copyright">Copyright © 2018. Tous droits réservés.</div>
                </div>
            </div>
        </footer>
        <script type="text/javascript" language="JavaScript">
            <?php if(isset($_SESSION['message']) && $_SESSION['message'] != null){?> window.alert("<?php echo $_SESSION['message'];?>"); <?php } ?>
        </script>

        <?php $_SESSION['message'] = null; ?>
    </body>
</html>			